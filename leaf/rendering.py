import logging
from abc import ABC, abstractmethod
from datetime import datetime, timezone

import imageio
import numpy as np
import torch

from leaf.ray_tracing import intersect_aabb


class RaySampler(ABC):
    @abstractmethod
    def sample(self, origins, directions, num_samples=None, near=0.0, far=1.0):
        return None


class LinearRaySampler(RaySampler):
    def __init__(self, num_samples=64, jitter=True, bounds=None):
        """
        :param num_samples: Number of samples to take along the ray
        :param jitter: if True, randomly jitter each sample position in the interval [sample_n, sample_n + 1)
        :param bounds: If not None, limit all samples to the given bounding box
        """
        self._num_samples = num_samples
        self._jitter = jitter
        self._bounds = bounds

    @property
    def num_samples(self):
        return self._num_samples

    @num_samples.setter
    def num_samples(self, n: int):
        self._num_samples = n

    @property
    def jitter(self):
        return self._jitter

    @jitter.setter
    def jitter(self, b: bool):
        self._jitter = b

    def sample(self, origins, directions, num_samples=None, near=0.0, far=1.0):
        if num_samples is None:
            num_samples = self._num_samples

        if np.isscalar(near):
            near = np.full((origins.shape[0],), near)
        if np.isscalar(far):
            far = np.full((origins.shape[0],), far)

        # Update near/far if we have a bounding box
        if self._bounds is not None:
            b_min = self._bounds[0]
            b_max = self._bounds[1]
            hit, t1, t2 = intersect_aabb(origins, directions, b_min, b_max)
            near[hit] = np.fmax(t1, near)
            far[hit] = np.fmin(t2, far)
            # TODO: Non-intersecting rays still get sampled

        # sample ray
        samples = ray_sample_linear(origins, directions, num_samples, near, far,
                                    self._jitter)

        return samples


def ray_sample_linear(origins, directions, num_samples: int, near, far,
                      jitter: bool = False):
    """
    Linearly sample a batch of rays defined by an origin and direction on the interval [near, far]

    :param origins: np.ndarray of shape [rays, 3]
    :param directions: np.ndarray of shape [rays, 3]
    :param num_samples: number of samples to take
    :param near: starting distance from origin
    :param far: ending distance from origin
    :param jitter: if True, randomly jitter each sample in the interval [sample_n, sample_n + 1)
    :return: np.ndarray of shape [rays, num_samples]
    """
    z_vals = np.linspace(0, 1, num_samples, dtype=np.float32)
    z_vals = near * (1. - z_vals) + far * z_vals
    z_vals = np.broadcast_to(z_vals, (origins.shape[0], z_vals.shape[0]))

    if jitter:
        mids = .5 * (z_vals[..., 1:] + z_vals[..., :-1])
        upper = np.concatenate([mids, z_vals[..., -1:]], -1)
        lower = np.concatenate([z_vals[..., :1], mids], -1)
        t_rand = torch.rand(z_vals.shape, dtype=torch.float32).numpy()
        z_vals = lower + (upper - lower) * t_rand

    return origins[..., None, :] + directions[..., None, :] * z_vals[..., :,
                                                              None]


def render_rays(pts, model, expected=None, loss_fn=None) -> dict:
    """
    Evaluate points sampled from a batch of projected rays and integrates the
    results to produce a batch of projected pixels.

    Note: All inputs are expected to be on the same compute device

    :param pts: Tensor of shape (batch size, num samples, model input channels)
    :param model: Model to use for evaluating pts
    :param expected: Expected pixel value after integration
    :param loss_fn: Loss function for comparing inferred to expected
    :return: dict of coeffs: Tensor of inferred values, predicted: Tensor of
    projected pixels, loss: calculated loss if expected and loss_fn are provided
    """
    # Query network for attenuation coefficients
    # pts = (batch, samples, 3)
    coeffs = model(pts)

    # Integrate each ray
    predicted = 1. - torch.sum(coeffs, dim=1)

    # Calculate loss
    loss = None
    if expected is not None and loss_fn is not None:
        loss = loss_fn(predicted, expected)

    return {"coeffs": coeffs, "predicted": predicted, "loss": loss}


def render_voxels(pts, model, expected=None, loss_fn=None) -> dict:
    """
    Evaluate a batch of points sampled from voxel space.

    Note: All inputs are expected to be on the same compute device

    :param pts: Tensor of shape (batch size, num samples, model input channels)
    :param model: Model to use for evaluating pts
    :param expected: Expected voxel values
    :param loss_fn: Loss function for comparing inferred to expected
    :return: dict of coeffs: Tensor of inferred values, predicted: Same as
    coeffs, loss: calculated loss if expected and loss_fn are provided
    """
    # Query network for attenuation coefficients
    coeffs = model(pts)

    # Calculate loss
    loss = None
    if expected is not None and loss_fn is not None:
        loss = loss_fn(coeffs, expected)

    return {"coeffs": coeffs, "predicted": coeffs, "loss": loss}


def render_images(dataloader, device, model, shape, buffer_max=1024,
                  render_fn=render_rays, flush_path=None, log_interval=None):
    logger = logging.getLogger(__name__)
    model.eval()
    with torch.no_grad():
        # Variables for tracking info about the rendering session
        total_batches = len(dataloader)
        pixel_buffer = None
        all_images = None

        def flush_pixel_buffer(buffer, images):
            num_complete_images = buffer.size // np.prod(shape[1:])
            complete_shape = (num_complete_images,) + shape[1:]
            complete_pixels = np.prod(complete_shape)
            complete_images = buffer[:complete_pixels].reshape(complete_shape)
            if images is None:
                images = complete_images
            else:
                images = np.append(images, complete_images, axis=0)
            buffer = buffer[complete_pixels:]
            if flush_path is not None:
                imageio.mvolwrite(flush_path, images)
                logger.debug(f'Saved {images.shape[0]} buffered images')
            return buffer, images

        for batch_idx, pts in enumerate(dataloader):
            # Render batch
            batch_start = datetime.now(timezone.utc)
            pixels = render_fn(pts=pts.to(device), model=model)["predicted"]
            if pixel_buffer is None:
                pixel_buffer = pixels.cpu().numpy()
            else:
                pixel_buffer = np.append(pixel_buffer, pixels.cpu().numpy(),
                                         axis=0)
            batch_duration = (datetime.now(
                timezone.utc) - batch_start).total_seconds()

            # Log as needed
            if log_interval is not None and batch_idx % log_interval == 0:
                logger.info(f'Rendered batch {batch_idx}/{total_batches} '
                            f'({batch_duration:5.3g}s)')

            # Write complete images as needed
            if pixel_buffer.size > buffer_max:
                pixel_buffer, all_images = flush_pixel_buffer(
                    buffer=pixel_buffer, images=all_images)
    model.train()

    # Always do one last buffer flush
    if pixel_buffer.size > 0:
        pixel_buffer, all_images = flush_pixel_buffer(buffer=pixel_buffer,
                                                      images=all_images)

    return all_images, pixel_buffer
