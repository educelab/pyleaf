import logging
from functools import partial

import imageio
import numpy as np
from phantominator import ct_shepp_logan
from scipy import ndimage
from skimage.transform import rescale
from tqdm import tqdm

from leaf.ray_tracing import ParallelBeamProjector, normalize, rotate


def _rotate_volume_z(image, theta):
    """Rotates a 3D image (x,y,z) theta radians around the z axis"""
    if theta == 0.0 or theta % (2. * np.pi) == 0.0:
        return image
    height = np.arange(image.shape[0], dtype=np.float32)
    width = np.arange(image.shape[1], dtype=np.float32)
    slices = np.arange(image.shape[2], dtype=np.float32)
    y_, x_, z_ = np.meshgrid(height / image.shape[0] - 0.5,
                             width / image.shape[1] - 0.5,
                             slices / image.shape[2] - 0.5, indexing='ij')
    x_rot = x_ * np.cos(theta) - y_ * np.sin(theta)
    y_rot = x_ * np.sin(theta) + y_ * np.cos(theta)
    x_rot = (x_rot + 0.5) * image.shape[1]
    y_rot = (y_rot + 0.5) * image.shape[0]
    z_rot = (z_ + 0.5) * image.shape[2]
    sample_coords = np.stack([y_rot, x_rot, z_rot], axis=0)
    return ndimage.map_coordinates(image, sample_coords, order=3).reshape(
        image.shape)


def _ct_project(image):
    res = np.mean(image, axis=0)
    return np.swapaxes(res, 0, 1)


def load_phantom(phantom_gen, dim=192, offset=0, skip=1, scale=None,
                 theta_start=0., theta_end=2. * np.pi,
                 num_projs=360, load_images=True, single_slice=False, **kwargs):
    # Camera parameters
    source_to_sample = 1.1 * dim / 2
    source_to_detector = 2. * source_to_sample
    detector_pix_size = pix_size = 0.05

    # Scale the sensor pixel size
    if scale is not None:
        pix_size /= scale

    # Create the projectors
    projectors = []
    thetas = np.linspace(theta_start, theta_end, num_projs, endpoint=False)
    volume = None
    if load_images:
        volume = phantom_gen(N=(512, 512, 512), zlims=(-1, 1))
        volume = ndimage.zoom(volume, zoom=dim / 512, order=3,
                              prefilter=(dim < 512))
    for theta in tqdm(thetas[offset:None:skip], desc='Generating projections'):
        # Calculate source position
        up = (0, 0, 1)
        src = np.array((0, -source_to_sample, 0), dtype=np.float32)
        src = rotate(theta, up) @ src

        # Get center of projection plane
        n = normalize(-src)
        lookat = src + n * source_to_detector

        # Load images if requested
        img = None
        size = None
        if load_images:
            slices_rot = _rotate_volume_z(volume, theta)
            img = _ct_project(image=slices_rot)
            img = 1. - img
            if scale is not None:
                img = rescale(img, scale=scale, anti_aliasing=True)

            # Crop to middle row(s)
            if single_slice:
                start = (img.shape[0] - 1) // 2
                if start % 2 == 0:
                    img = img[start:start + 2]
                else:
                    img = img[start:start + 1]

            # Reshape image
            s = (img.shape[0], img.shape[1], 1)
            img = img.reshape(s).astype(np.float32)
        else:
            width = dim
            height = dim
            if scale is not None:
                width = np.round(width * scale).astype(int)
                height = np.round(height * scale).astype(int)
            size = (height, width)

        # Add projector
        cam = ParallelBeamProjector(pos=src, lookat=lookat, up=up, image=img,
                                    size=size, pixel_size=pix_size)
        projectors.append(cam)

    # Calculate volume size
    vol_half = dim * detector_pix_size / 2.
    vol_min = np.array([-vol_half, -vol_half, -vol_half], dtype=np.float32)
    vol_max = np.array([vol_half, vol_half, vol_half], dtype=np.float32)

    return {
        'projectors': projectors,
        'volume': {
            'min': vol_min,
            'max': vol_max,
            'diameter': np.amax((vol_max - vol_min)[0:2]),
            'height': (vol_max - vol_min)[2]
        },
        'slices': {
            'cols': dim,
            'rows': dim,
            'pix-size': detector_pix_size
        }
    }


def load_shepp_logan_projections(offset=0, skip=1, scale=None, gen_dim=512,
                                 vol_dim=192, theta_start=0.,
                                 theta_end=2. * np.pi,
                                 num_projs=360):
    imgs = []
    thetas = np.linspace(theta_start, theta_end, num_projs)
    logging.info(f'Generating volume ({gen_dim}, {gen_dim}, {gen_dim})')
    volume = ct_shepp_logan((gen_dim, gen_dim, gen_dim), zlims=(-1, 1))
    logging.info(f'Scaling volume ({vol_dim}, {vol_dim}, {vol_dim})')
    volume = ndimage.zoom(volume, zoom=vol_dim / gen_dim, order=3,
                          prefilter=(vol_dim < gen_dim))
    for theta in tqdm(thetas[offset:None:skip], desc='Generating projections'):
        p = _rotate_volume_z(volume, theta)
        p = _ct_project(image=p)
        p = 1. - p
        if scale is not None:
            p = rescale(p, scale=scale, anti_aliasing=True)

        # Reshape image
        p = p.reshape([p.shape[0], p.shape[1], 1]).astype(np.float32)
        imgs.append(p)
    return np.array(imgs, dtype=np.float32)


def load_shepp_logan_slices(dim=192, modified=True):
    volume = ct_shepp_logan((512, 512, 512), zlims=(-1, 1), modified=modified)
    volume = ndimage.zoom(volume, zoom=dim / 512, order=3,
                          prefilter=(dim < 512))
    volume = np.transpose(volume, axes=(2, 0, 1))
    volume = np.flip(volume, axis=0)
    return volume.astype(np.float32)


def load_phantom_dataset(data_dir, **kwargs):
    logger = logging.getLogger(__name__)
    # Check for phantom protocol
    if data_dir.find('phantom://') != 0:
        raise RuntimeError(f'Unrecognized argument: {data_dir}')

    # Remove prefix
    phantom = data_dir.replace('phantom://', '')

    # Check for one of recognized types
    if phantom == 'shepp-logan':
        logger.debug(f'Loading phantom: Shepp-Logan')
        phantom_gen = partial(ct_shepp_logan, modified=False)
    elif phantom == 'shepp-logan-modified':
        logger.debug(f'Loading phantom: Shepp-Logan (Modified)')
        phantom_gen = ct_shepp_logan
    else:
        raise RuntimeError(f'Unrecognized phantom dataset: {phantom}')

    return load_phantom(phantom_gen, **kwargs)


def main():
    logging.basicConfig(level=logging.INFO)
    # Save projections
    projs = load_shepp_logan_projections()
    logging.info('Saving projections')
    imageio.mimwrite('projections.tif', projs)

    # Save slices
    logging.info('Generating slices')
    slices = load_shepp_logan_slices()
    logging.info('Saving slices')
    imageio.mimwrite('slices.tif', slices)


if __name__ == '__main__':
    main()
