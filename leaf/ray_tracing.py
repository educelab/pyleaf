import math
from typing import Tuple, Union

import numpy as np
import numpy.linalg as linalg


def normalize(v):
    d = np.sum(v ** 2)
    if d == 0:
        return v
    return v / np.sqrt(d)


def rotate(t, v):
    """Generate rotation matrix to rotate t radians around 3D vector v"""
    x, y, z = normalize(np.array(v, dtype=np.float32))
    s = np.sin(t)
    c = np.cos(t)
    r = np.eye(3)
    r[0, 0] = x * x * (1 - c) + c
    r[0, 1] = x * y * (1 - c) - z * s
    r[0, 2] = x * z * (1 - c) + y * s
    r[1, 0] = y * x * (1 - c) + z * s
    r[1, 1] = y * y * (1 - c) + c
    r[1, 2] = y * z * (1 - c) - x * s
    r[2, 0] = x * z * (1 - c) - y * s
    r[2, 1] = y * z * (1 - c) + x * s
    r[2, 2] = z * z * (1 - c) + c
    return r


def intersect_aabb(origin, direction, box_min, box_max):
    """
    Ray-box intersection adapted from:
    https://tavianator.com/2015/ray_box_nan.html
    """
    # This algorithm relies on IEE 754's divide-by-zero -> inf behavior
    with np.errstate(divide='ignore'):
        dir_inv = 1. / direction
    t1 = (box_min[..., 0] - origin[..., 0]) * dir_inv[..., 0]
    t2 = (box_max[..., 0] - origin[..., 0]) * dir_inv[..., 0]

    t_mins = np.fmin(t1, t2)
    t_maxs = np.fmax(t1, t2)

    for i in range(3):
        t1 = (box_min[..., i] - origin[..., i]) * dir_inv[..., i]
        t2 = (box_max[..., i] - origin[..., i]) * dir_inv[..., i]

        t_mins = np.fmax(t_mins, np.fmin(t1, t2))
        t_maxs = np.fmin(t_maxs, np.fmax(t1, t2))

    hits = t_maxs >= np.fmax(t_mins, np.zeros(t_mins.shape))
    if np.isscalar(t_mins) and not hits:
        t_mins = -np.inf
    elif not np.isscalar(t_mins):
        t_mins[np.invert(hits)] = -np.inf

    if np.isscalar(t_maxs) and not hits:
        t_maxs = -np.inf
    elif not np.isscalar(t_maxs):
        t_maxs[np.invert(hits)] = -np.inf
    return hits, t_mins, t_maxs


class ParallelBeamProjector:
    def __init__(self, pos, lookat, up, image=None, size=None,
                 pixel_size: Union[float, Tuple[float, float]] = 1.):
        """
        Parallel projection X-ray camera
        @param pos: Source position in world coordinates (mm)
        @param lookat: Projection plane center in world coordinates (mm)
        @param up: Up orientation unit vector
        @param image: Image captured from this camera
        @param size: Dimensions of image plane (height, width) in pixels. Required if image argument not provided
        @param pixel_size: Pixel size (mm). If a tuple is provided, pixels are considered anisotropic with size (height, width). If a single value is provided, the pixels are considered isotropic
        """

        self._pos = np.array(pos)
        self._lookat = np.array(lookat)
        self._up = np.array(up)
        self._img = image

        # Get the image plane discrete size
        if image is not None:
            size = image.shape[0:2]
        if size is None:
            raise RuntimeError('Must provide argument image or size')
        self._proj_size = size

        if type(pixel_size) is not tuple:
            self._pix_size = (pixel_size, pixel_size)
        else:
            self._pix_size = pixel_size[0:2]

        w = normalize(self._pos - self._lookat)
        u = normalize(np.cross(self._up, w))
        v = normalize(np.cross(w, u))

        self._horz = u * self._proj_size[1] * self._pix_size[1]
        self._vert = v * self._proj_size[0] * self._pix_size[0]
        self._llc = self._pos - (self._horz / 2) - (self._vert / 2) - (
                self._pos - self._lookat)

    @property
    def pos(self):
        return self._pos

    @property
    def proj_dist(self):
        """
        @return: Distance of projection plane from source
        """
        return linalg.norm(self._lookat - self._pos)

    @property
    def image(self):
        return self._img

    @property
    def proj_size(self):
        """
        Projection size in pixels (height, width)
        @return:
        """
        return self._proj_size

    @property
    def num_pixels(self) -> int:
        """
        Number of discrete samples in this projection
        """
        return self.proj_size[0] * self.proj_size[1]

    def _val(self, s, t):
        """
        Return the intensity value for position (s, t) in the range [0, 1]
        """
        val = None
        if self._img is not None:
            x = int(math.floor(s * (self.proj_size[1] - 1)))
            y = int(math.floor(t * (self.proj_size[0] - 1)))
            if 0 <= x < self.proj_size[1] and 0 <= y < self.proj_size[0]:
                val = self._img[y, x]
        return val

    def ray(self, s, t) -> Tuple[np.array, np.array, float, float]:
        """
        Get ray for coordinate on projection plane
        @param s: Horizontal component relative to top-left corner in range
        [0, 1]
        @param t: Vertical component relative to top-left corner in range [0, 1]
        @return: Tuple of (ray origin, ray direction, distance from the origin
        to the projection plane, image intensity value)
        """
        v = self._lookat - self._pos
        intersection = self._llc + s * self._horz + (1 - t) * self._vert
        pos = intersection - v
        val = self._val(s, t)
        return pos, normalize(v), linalg.norm(v), val

    def ray_pixel(self, x, y) -> Tuple[np.array, np.array, float, float]:
        """
        Get ray for pixel on projection plane
        @param x: Horizontal position of pixel in range [0, width]
        @param y: Vertical position of pixel in range [0, height]
        @return: Tuple of (ray origin, ray direction, distance from the origin
        to the projection plane, image intensity value)

        In world coordinates, the returned ray intersects the center of the
        pixel
        """
        if self.proj_size[1] == 1:
            u = 0.5
        else:
            off = 0.5  # random.random()
            u = (x + off) / (self.proj_size[1] - 1)

        if self.proj_size[0] == 1:
            v = 0.5
        else:
            off = 0.5  # random.random()
            v = (y + off) / (self.proj_size[0] - 1)

        return self.ray(u, v)

    def ray_index(self, i) -> Tuple[np.array, np.array, float, float]:
        """
        Get ray for pixel on projection plane. Same as ray_pixel(), but for flat
        indices.
        :param i: Flat index for pixel
        :return: Tuple of (ray origin, ray direction, distance from the origin
        to the projection plane, image intensity value)
        """
        y, x = np.unravel_index(i, self.proj_size)
        return self.ray_pixel(x, y)


class ConeBeamProjector(ParallelBeamProjector):
    def __init__(self, **kwargs):
        """Perspective projection X-ray camera"""
        super(ConeBeamProjector, self).__init__(**kwargs)

    def ray(self, s, t) -> Tuple[np.array, np.array, float, float]:
        """
        Get ray for coordinate on projection plane
        @param s: Horizontal component relative to top-left corner in range
        [0, 1]
        @param t: Vertical component relative to top-left corner in range [0, 1]
        @return: Tuple of (ray origin, ray direction, distance from the origin
        to the projection plane, image intensity value)
        """
        v = self._llc + s * self._horz + (1 - t) * self._vert - self._pos
        val = self._val(s, t)
        return self.pos, normalize(v), linalg.norm(v), val
