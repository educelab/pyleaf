import unittest
from functools import partialmethod

import numpy as np
from tqdm import tqdm

# Disable all tqdm calls inside imported libraries
tqdm.__init__ = partialmethod(tqdm.__init__, disable=True)


class TestDatasets(unittest.TestCase):
    def test_get_dataset_type(self):
        from leaf.datasets.datasets import _get_dataset_type, DatasetType
        t = _get_dataset_type('phantom://shepp-logan')
        self.assertEqual(t, DatasetType.PHANTOM)

    # Disabled: Times out on CI runners
    # def test_load_dataset_phantom(self):
    #     from leaf.datasets import load_dataset
    #     data = load_dataset('phantom://shepp-logan', dim=64, num_projs=64)
    #     self.assertEqual(len(data['projectors']), 64)


class TestData(unittest.TestCase):
    def test_permute(self):
        from leaf.data import permute
        # Permute list
        a = [0, 1, 2, 3, 4]
        b = permute(a, (1, 2, 3, 4, 0))
        self.assertEqual(type(a), type(b))
        self.assertEqual([1, 2, 3, 4, 0], b)

        # Permute tuple
        a = (0, 1, 2, 3, 4)
        b = permute(a, (1, 2, 3, 4, 0))
        self.assertEqual(type(a), type(b))
        self.assertEqual((1, 2, 3, 4, 0), b)

    def test_MinMaxScaler_1D(self):
        from leaf.data import MinMaxScaler
        scaler = MinMaxScaler(in_bounds=[[0., 10.], [0, 20]],
                              out_bounds=[[-1., 1.], [-1, 1]])
        self.assertTrue(np.array_equal(scaler.transform([0, 0]), [-1., -1.]))
        self.assertTrue(np.array_equal(scaler.transform([5, 10]), [0., 0.]))
        self.assertTrue(np.array_equal(scaler.transform([10, 20]), [1., 1.]))

    def test_MinMaxScaler_2D(self):
        from leaf.data import MinMaxScaler
        scaler = MinMaxScaler(in_bounds=[[0., 10.], [0, 20]],
                              out_bounds=[[-1., 1.], [-1, 1]])
        x = [[0, 0], [5, 10], [10, 20]]
        expected = [[-1, -1], [0, 0], [1, 1]]
        self.assertTrue(np.array_equal(scaler.transform(x), expected))

    def test_ProjectorDataset(self):
        from leaf.ray_tracing import ConeBeamProjector
        from leaf.rendering import LinearRaySampler
        from leaf.data import ProjectorDataset
        box_min = np.array((-10, -5, -10))
        box_max = np.array((10, -2, 10))
        sampler = LinearRaySampler(jitter=False,
                                   bounds=(box_min, box_max))
        img = np.arange(0, 100).reshape((10, 10))
        cbp = ConeBeamProjector(pos=(0, -10, 0), lookat=(0, 0, 0), up=(0, 0, 1),
                                image=img)
        dataset = ProjectorDataset(projectors=[cbp], ray_sampler=sampler)

        for idx in range(len(dataset)):
            samples, val = dataset[idx]
            self.assertEqual(val, idx)

    def test_SlicesDataset(self):
        from leaf.data import SlicesDataset
        dataset = SlicesDataset(centers=np.array([[0., 0., 0.], [0., 0., 1.]]),
                                extents=np.array([[10.0, 10.0], [10.0, 10.0]]),
                                width=5,
                                height=5)

        expected = []
        for z in range(2):
            for y in range(-4, 6, 2):
                for x in range(-4, 6, 2):
                    expected.append(np.array([x, y, z], dtype=float))

        for idx in range(0, len(dataset)):
            self.assertTrue(np.equal(dataset[idx], expected[idx]).all())


class TestEmbedding(unittest.TestCase):
    def test_MinMaxScalingEmbedder(self):
        from leaf.embedding import MinMaxScalingEmbedder
        from torch import all, tensor, eq
        x_min, x_max = tensor([-0.5, -0.5, -0.5]), tensor([0.5, 0.5, 0.5])
        a, b = tensor([0., 0., 0.]), tensor([1., 1., 1.])
        embedder = MinMaxScalingEmbedder(x_min=x_min, x_max=x_max, a=a, b=b)
        x = tensor([[-0.5, -0.5, -0.5], [0, 0, 0], [0.5, 0.5, 0.5]])
        output = embedder(x)
        expected = tensor([[0, 0, 0], [0.5, 0.5, 0.5], [1, 1, 1]])
        self.assertTrue(all(eq(output, expected)))


class TestModules(unittest.TestCase):
    def test_Transform3D(self):
        from leaf.modules import Transform3D
        import torch
        from torch import all, tensor, eq

        # Setup translation transform
        m = torch.eye(4)
        m[:, 3] = torch.tensor([0.5, 0.5, 0.5, 1])
        tfm = Transform3D(mtx=m)

        # Run inputs
        x = tensor([[0, 0, 0], [0.5, 0.5, 0.5], [1, 1, 1]])
        output = tfm(x)

        # Compare outputs
        expected = tensor([[0.5, 0.5, 0.5], [1, 1, 1], [1.5, 1.5, 1.5]])
        self.assertTrue(all(eq(output, expected)))


class TestPoseHelpers(unittest.TestCase):
    def test_translation(self):
        from leaf.pose_helpers import translation
        tfm = translation(5, 6, 7)
        result = tfm @ np.array([[0], [0], [0], [1]])
        expected = np.array([[5], [6], [7], [1]])
        self.assertTrue(np.equal(result, expected).all())


class TestRayTracing(unittest.TestCase):
    def test_normalize(self):
        from leaf.ray_tracing import normalize
        x = np.array((0., 10., 0.))
        y = normalize(x)
        expected = np.array((0., 1., 0.))
        self.assertTrue(np.equal(y, expected).all())

    def test_rotate(self):
        from leaf.ray_tracing import rotate
        x = np.array((1, 0, 0))
        expected = np.array((0, 1, 0))
        y = rotate(np.deg2rad(90), (0, 0, 1)) @ x
        self.assertTrue(np.allclose(y, expected))

    def test_intersect_box(self):
        from leaf.ray_tracing import intersect_aabb
        box_min = np.array((-1, -1, -1))
        box_max = np.array((1, 1, 1))

        # Intersection single
        e = np.array([0, -2, 0])
        d = np.array([0, 1, 0])
        res = intersect_aabb(e, d, box_min, box_max)
        self.assertEqual(res, (True, 1.0, 3.0))

        # Intersection batch (outside, inside, none)
        e = np.array([[0, -2, 0], [0, 0, 0], [2, 0, 0]])
        d = np.array([[0, 1, 0], [0, 1, 0], [0, 1, 0]])
        hits, t1s, t2s = intersect_aabb(e, d, box_min, box_max)
        self.assertTrue(np.array_equal(hits, (True, True, False)))
        self.assertTrue(np.array_equal(t1s, (1., -1., -np.inf)))
        self.assertTrue(np.array_equal(t2s, (3., 1., -np.inf)))

    def test_ParallelBeamProjector_NoImg(self):
        from leaf.ray_tracing import ParallelBeamProjector
        # X+right, Y-forward, Z+up
        pos = (0, -1, 0)
        lookat = (0, 0, 0)
        up = (0, 0, 1)
        cam = ParallelBeamProjector(pos=pos, lookat=lookat, up=up, size=(1, 1))
        for t in np.linspace(0, 1, num=5):
            for s in np.linspace(0, 1, num=5):
                o, d, dist, _ = cam.ray(s, t)
                pt = np.add(o, d * dist)
                expected = np.array((s - 0.5, 0., -t + 0.5))
                self.assertTrue(np.equal(pt, expected).all())

    def test_ConeBeamProjector_NoImg(self):
        from leaf.ray_tracing import ConeBeamProjector
        # X+right, Y-forward, Z+up
        pos = (0, -1, 0)
        lookat = (0, 0, 0)
        up = (0, 0, 1)
        cam = ConeBeamProjector(pos=pos, lookat=lookat, up=up, size=(1, 1))
        for t in np.linspace(0, 1, num=5):
            for s in np.linspace(0, 1, num=5):
                o, d, dist, _ = cam.ray(s, t)
                pt = np.add(o, d * dist)
                expected = np.array((s - 0.5, 0., -t + 0.5))
                self.assertTrue(np.equal(pt, expected).all())


if __name__ == '__main__':
    unittest.main()
