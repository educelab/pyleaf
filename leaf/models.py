import importlib
from collections import OrderedDict
from typing import Union

import numpy as np
import torch
from torch import nn

from leaf.embedding import PassThruEmbedder
from leaf.modules import Sin


def save_checkpoint(path, epoch, batch, model, optimizer):
    torch.save({
        'epoch': epoch,
        'batch': batch,
        'model_state_dict': model.state_dict(),
        'model_config': model.configuration,
        'optimizer_state_dict': optimizer.state_dict(),
    }, path)


def restore_model_from_checkpoint(checkpoint, device):
    # TODO: This is currently broken
    depth = checkpoint['model_config']['hidden_layers']
    width = checkpoint['model_config']['hidden_cns']

    activation_id = checkpoint['model_config']['activation']
    # Handle fully qualified names
    if '.' in activation_id:
        activation_module, activation_name = activation_id.rsplit('.', 1)
    # Otherwise, it's an old model which only had the class name
    else:
        activation_module = 'torch.nn'
        activation_name = activation_id
        if activation_name == 'type':
            activation_name = 'ReLU'
    module = importlib.import_module(activation_module)
    activation_type = getattr(module, activation_name)

    embedder_type = getattr(importlib.import_module('leaf.embedding'),
                            checkpoint['model_config']['embedder'])

    # Create model
    model = Leaf(hidden_layers=depth, hidden_cns=width,
                 embedder=embedder_type(), activation_fn=activation_type).to(
        device)
    model.load_state_dict(checkpoint['model_state_dict'])
    optimizer = torch.optim.Adam(params=model.parameters())
    optimizer.load_state_dict(checkpoint['optimizer_state_dict'])

    return model, optimizer


class Leaf(nn.Module):

    def __init__(self, input_cns=3, output_cns=1, hidden_layers=8,
                 hidden_cns=256,
                 embedder: Union[nn.Module, nn.Sequential] = PassThruEmbedder(),
                 activation_fn=nn.ReLU,
                 sin_w0=30):
        super(Leaf, self).__init__()

        # Get the size of the embedder
        if type(embedder) == nn.Sequential:
            *_, last = embedder.children()
            embed_cns_in = last.input_cns
            embed_cns_out = last.output_cns
        else:
            embed_cns_in = embedder.input_cns
            embed_cns_out = embedder.output_cns

        assert input_cns == embed_cns_in, f'input_cns {input_cns} doesn\'t match embedder input_cns {embed_cns_in}'

        # Keep for posterity
        self._num_input_cns = input_cns
        self._num_output_cns = output_cns
        self._activation = activation_fn
        self._num_hidden_layers = hidden_layers
        self._num_hidden_channels = hidden_cns
        self._embedder = embedder

        # Set up the activation kwargs and label
        activ_label = f'{self._activation.__name__}'
        activ_kwargs = {}
        if self._activation is Sin:
            activ_kwargs['period'] = sin_w0

        # List of layers in MLP
        layers = []

        # Hidden layers
        in_cns = embed_cns_out
        for layer_idx in range(hidden_layers - 1):
            # construct the layer
            layer = nn.Linear(in_cns, hidden_cns)

            # Implicit Neural Representations' initialization scheme
            if self._activation is Sin:
                with torch.no_grad():
                    if layer_idx == 0:
                        r = 1. / in_cns
                        nn.init.uniform_(layer.weight, -r, r)
                    else:
                        r = np.sqrt(6. / in_cns) / sin_w0
                        nn.init.uniform_(layer.weight, -r, r)

            # Add the layer to the layer list
            layers.append((f'linear_{layer_idx}', layer))
            layers.append((f'{activ_label}_{layer_idx}',
                           self._activation(**activ_kwargs)))

            # Next layer's inputs are this layers outputs
            in_cns = hidden_cns

        # Final output layer
        output = nn.Linear(hidden_cns, output_cns)
        if self._activation is Sin:
            with torch.no_grad():
                r = np.sqrt(6. / in_cns) / sin_w0
                nn.init.uniform_(output.weight, -r, r)
        layers.append((f'output', output))
        self._mlp = nn.Sequential(OrderedDict(layers))

    def forward(self, x):
        x = self._embedder(x)
        x = self._mlp(x)
        return x

    @property
    def num_input_cns(self):
        return self._num_input_cns

    @property
    def num_output_cns(self):
        return self._num_output_cns

    @property
    def num_hidden_layers(self):
        return self._num_hidden_layers

    @property
    def num_hidden_channels(self):
        return self._num_hidden_channels

    @property
    def configuration(self):
        return {
            'input_cns': self._num_input_cns,
            'output_cns': self._num_output_cns,
            'hidden_layers': self._num_hidden_layers,
            'hidden_cns': self._num_hidden_channels,
            'activation': f'{self._activation.__module__}.{self._activation.__name__}',
            'embedder': self._embedder.__class__.__name__
        }
