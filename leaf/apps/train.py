import logging
import sys
from datetime import datetime as dt, timezone as tz
from distutils.util import strtobool
from pathlib import Path

import bitmath
import configargparse
import numpy as np
import torch
import torchsummary
from bitmath import Byte
from torch.nn import ReLU
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter

import leaf.log_utils as log_utils
from leaf.data import PosesDataset, ProjectionsDataset, SlicesDataset, \
    build_spherical_render_poses, random_volume_points
from leaf.datasets.skyscan import load_skyscan_datasets, load_skyscan_metadata
from leaf.embedding import PassThruEmbedder, PositionalEncoder
from leaf.models import Leaf, save_checkpoint
from leaf.modules import Sin
from leaf.rendering import LinearRaySampler, render_images, render_rays, \
    render_voxels


class MaxBatchReached(Exception):
    """Indicates that the user-defined batch maximum has been reached"""


def evaluate_test_batch(dataloader, model, loss_fn, device) -> dict:
    """
    Evaluates the first batch of a DataLoader using the provided model and no
    gradient recording.
    """
    model.eval()
    with torch.no_grad():
        # Test against one batch of pixels
        test_px, test_pts = next(iter(dataloader))
        result = render_rays(pts=test_pts.to(device), model=model,
                             expected=test_px.to(device), loss_fn=loss_fn)
    model.train()
    return result


def main():
    # Parse arguments
    parser = configargparse.ArgumentParser(prog='pyleaf-train')

    # Generic options
    parser.add_argument('-c', '--config', is_config_file=True,
                        help='Config file path')
    parser.add_argument('-i', '--input', type=str, required=True,
                        help='Path to input dataset')
    parser.add_argument('-o', '--output', type=str,
                        help='Path to output directory for results. If not '
                             'specified, a results directory will be '
                             'created in the current working directory')
    parser.add_argument('-n', '--name', type=str, help='Experiment name.')
    parser.add_argument('--log', type=str,
                        help='Write experiment log to a specific file. If not '
                             'specified, log will be written to output '
                             'directory')

    # Dataset options
    dataset_args = parser.add_argument_group('dataset options')
    dataset_args.add_argument('--scale', type=float,
                              help='Scale dataset images by the given factor')
    dataset_args.add_argument('--center-only', action='store_true',
                              default=False,
                              help='Only load the central row of pixels for '
                                   'each projection. This simulates a fanbeam '
                                   'geometry. If the projection height is '
                                   'even, the central two rows will be loaded.')
    dataset_args.add_argument('--train-offset', type=int, default=0,
                              help='When loading a training dataset, ignore '
                                   'the first --train-offset images')
    dataset_args.add_argument('--train-skip', type=int, default=1,
                              help='When loading a training dataset, only '
                                   'include every --train-skip images')
    dataset_args.add_argument('--test-offset', type=int, default=0,
                              help='When loading a testing dataset, ignore '
                                   'the first --test-offset images')
    dataset_args.add_argument('--test-skip', type=int, default=1,
                              help='When loading a testing dataset, only '
                                   'include every --test-skip images')

    # Model options
    network_args = parser.add_argument_group('model options')
    network_args.add_argument('-d', '--depth', type=int, default=8,
                              help='Number of hidden layers')
    network_args.add_argument('-w', '--width', type=int, default=256,
                              help='Number of neurons in each hidden layer')
    network_args.add_argument('-s', '--samples', type=int, default=64,
                              help='Number of ray samples')
    network_args.add_argument('--activation', type=str.lower, default='relu',
                              choices=['relu', 'sin'],
                              help='Hidden layer activation function')
    network_args.add_argument('--sin-w0-value', type=float, default='30',
                              help='When using Sin activation, set the value '
                                   'of the weights in the first layer')
    network_args.add_argument('--embedder', type=str.lower, default='nerf',
                              choices=['none', 'nerf'],
                              help='Point embedding function')
    network_args.add_argument('--init-val', type=float,
                              help='Initial volume value')
    network_args.add_argument('--init-threshold', type=float, default=5e-8,
                              help='Loss threshold for volume initialization')

    # Training options
    training_args = parser.add_argument_group('training options')
    training_args.add_argument('-e', '--epochs', type=int, default=1000,
                               help='Training epochs')
    training_args.add_argument('-b', '--batch-size', type=int, default=1024,
                               help='Mini-batch size')
    training_args.add_argument('--shuffle', default=True,
                               type=lambda x: bool(strtobool(x)),
                               help='If enabled, shuffle the training samples')
    training_args.add_argument('--permute', action='store_true', default=False,
                               help='If enabled, train on a permutation of the '
                                    'training projections and poses')
    training_args.add_argument('--batches', type=int,
                               help='If specified, stop training after a '
                                    'maximum of N batches')
    training_args.add_argument('--learning-rate', type=float, default=1.e-3,
                               help='Optimizer learning rate')

    # Tensorboard options
    tb_args = parser.add_argument_group('tensorboard options')
    tb_args.add_argument('--tb-train-summary', type=int, default=10,
                         help='Log training summary to Tensorboard every N '
                              'batches')
    tb_args.add_argument('--tb-test-summary', type=int, default=10,
                         help='Log testing summary to Tensorboard every N '
                              'batches')
    tb_args.add_argument('--tb-test-image', type=int, default=100,
                         help='Log a test image to Tensorboard every N batches')

    # Rendering options
    render_args = parser.add_argument_group('rendering options')
    render_args.add_argument('--render-test-poses', type=int, default=5000,
                             help='Render the full test set every N batches. '
                                  'Disabled if N=0.')
    render_args.add_argument('--render-custom-poses', type=int, default=5000,
                             help='Render a custom spherical set every N '
                                  'batches. Disabled if N=0.')
    render_args.add_argument('--render-slices', type=int, default=5000,
                             help='Render the slice set every N batches. '
                                  'Disabled if N=0.')
    render_args.add_argument('--buffer-size', type=str, default='100M',
                             help='Pixel buffer size. Rendered images will be '
                                  'written to disk when the pixel buffer is '
                                  'full or the rendering job is complete.')

    args, unknown_args = parser.parse_known_args()

    # Setup experiment
    experiment_start = dt.now(tz.utc)
    datetime_str = experiment_start.strftime('%Y%m%d%H%M%S')
    if args.name is None:
        args.name = datetime_str + '_' + str(Path(args.input).stem)

    # Setup output directory
    if args.output is None:
        args.output = 'results/'
    experiment_output_dir = Path(args.output) / args.name
    log_dir = experiment_output_dir / 'logs'
    conf_dir = experiment_output_dir / 'configs'
    model_dir = experiment_output_dir / 'models'
    tensorboard_dir = experiment_output_dir / 'tensorboard' / datetime_str
    renders_dir = experiment_output_dir / 'renders' / datetime_str
    for d in [experiment_output_dir, log_dir, conf_dir, model_dir,
              tensorboard_dir, renders_dir]:
        d.mkdir(parents=True, exist_ok=True)

    # Setup logging
    if args.log is None:
        args.log = str(
            log_dir / (datetime_str + '_' + args.name + '_train_log.txt'))
    log_utils.setup_app_handlers(log_file=args.log)
    logger = logging.getLogger('leaf.train')
    logger.info(f'Set up output directory: {experiment_output_dir}')

    # Setup buffer flush size
    try:
        buffer_max = np.floor(
            bitmath.parse_string_unsafe(args.buffer_size) / Byte(4)).astype(int)
    except ValueError as e:
        logger.error(e)
        sys.exit(1)
    logger.debug(
        f'Pixel buffer size: '
        f'{(buffer_max * Byte(4)).best_prefix().format("{value:.3f}{unit}")}, '
        f'length: {buffer_max} pixels')

    # Write config after all arguments have been changed
    config = conf_dir / (datetime_str + '_' + args.name + '_train_config.txt')
    args.config = str(config)
    logger.debug(f'Writing config file: {args.config}')
    with config.open(mode='w') as file:
        for arg in vars(args):
            attr = getattr(args, arg)
            arg = arg.replace('_', '-')
            file.write('{} = {}\n'.format(arg, attr))

    # Get device
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    # Setup activation function
    if args.activation == 'sin':
        activation = Sin
    else:
        activation = ReLU

    # Setup embedder
    if args.embedder == 'nerf':
        # Load dataset metadata so we can init embedder properly
        meta = load_skyscan_metadata(args.input)
        w = meta['Acquisition']['Number of Columns']
        h = meta['Acquisition']['Number of Rows']
        pix_size = meta['Acquisition']['Image Pixel Size (um)'] / 1000.0
        vol_radius = w * pix_size / 2.
        vol_half_h = h * pix_size / 2.
        x_min = torch.tensor([-vol_radius, -vol_half_h, -vol_radius],
                             dtype=torch.float32, device=device)
        x_max = torch.tensor([vol_radius, vol_half_h, vol_radius],
                             dtype=torch.float32, device=device)
        embedder = PositionalEncoder(x_range=(x_min, x_max))
    else:
        embedder = PassThruEmbedder()

    # Create model
    model = Leaf(hidden_layers=args.depth, hidden_cns=args.width,
                 embedder=embedder, activation_fn=activation,
                 sin_w0=args.sin_w0_value).to(device)

    # Setup optimizer
    optimizer = torch.optim.Adam(params=model.parameters(),
                                 lr=args.learning_rate)

    # Setup loss
    loss_fn = torch.nn.MSELoss()

    # Preallocate model buffers
    pts = torch.rand((args.batch_size, args.samples, model.num_input_cns),
                     device=device)
    pixels = torch.rand((args.batch_size, 1), device=device)
    loss = render_rays(pts=pts, model=model, expected=pixels,
                       loss_fn=loss_fn)["loss"]
    loss.backward()
    model.zero_grad(set_to_none=True)

    # Report CUDA usage
    logger.info(f'PyTorch device: {device}')
    if device.type == 'cuda':
        logger.info(f'\t{torch.cuda.get_device_name(0)}')
        logger.info(
            f'\tMemory Allocated: {round(torch.cuda.memory_allocated(0) / 1024 ** 3, 1)} GB')
        logger.info(
            f'\tMemory Cached:    {round(torch.cuda.memory_reserved(0) / 1024 ** 3, 1)} GB')
    pin_mem = device.type == 'cuda'

    # Report model summary
    input_size = (args.batch_size, args.samples, model.num_input_cns)
    summary = torchsummary.summary(model=model,
                                   input_data=input_size,
                                   branching=False,
                                   device=device,
                                   verbose=0)
    logger.info(f'Model summary (sizes represent a single training batch):\n'
                f'{summary}')

    # Load datasets
    logger.info(f'Loading dataset: {args.input}')

    # log the first projection image if not doing center slice only
    log_img_indices = None
    if not args.center_only:
        log_img_indices = np.array([0])
    data, meta = load_skyscan_datasets(data_dir=args.input,
                                       scale_factor=args.scale,
                                       train_offset=args.train_offset,
                                       train_skip=args.train_skip,
                                       test_offset=args.test_offset,
                                       test_skip=args.test_skip,
                                       center_only=args.center_only,
                                       log_img_indices=log_img_indices)

    # Training data
    train_data = ProjectionsDataset(images=data['train']['images'],
                                    poses=data['train']['poses'],
                                    focal_length=data['focal_length'],
                                    near=data['near'], far=data['far'],
                                    permute=args.permute)
    train_dl = DataLoader(dataset=train_data, batch_size=args.batch_size,
                          shuffle=args.shuffle, pin_memory=pin_mem)

    # Test data
    test_data = ProjectionsDataset(images=data['test']['images'],
                                   poses=data['test']['poses'],
                                   focal_length=data['focal_length'],
                                   near=data['near'], far=data['far'])
    test_dl = DataLoader(dataset=test_data, batch_size=args.batch_size,
                         shuffle=True, pin_memory=pin_mem)

    # Log images data (poses only)
    log_shape = None
    log_data = None
    log_dl = None
    if 'log' in data.keys():
        log_shape = data['log']['images'].shape
        log_data = PosesDataset(poses=data['log']['poses'], height=log_shape[1],
                                width=log_shape[2],
                                focal_length=data['focal_length'],
                                near=data['near'], far=data['far'])
        log_dl = DataLoader(dataset=log_data, batch_size=args.batch_size,
                            pin_memory=pin_mem)

    # Log images data (slices only)
    slices_shape = None
    slices_data = None
    slices_dl = None
    if 'slices' in data.keys():
        slices = data['slices']['centers'].shape[0]
        height = data['slices']['height']
        width = data['slices']['width']
        slices_shape = (slices, height, width)
        slices_data = SlicesDataset(centers=data['slices']['centers'],
                                    extents=data['slices']['extents'],
                                    height=height,
                                    width=width)
        slices_dl = DataLoader(dataset=slices_data, batch_size=args.batch_size,
                               pin_memory=pin_mem)

    # Test data for full rendering (poses only)
    if args.render_test_poses > 0:
        render_test_shape = data['test']['images'].shape
        render_test_data = PosesDataset(poses=data['test']['poses'],
                                        height=render_test_shape[1],
                                        width=render_test_shape[2],
                                        focal_length=data['focal_length'],
                                        near=data['near'], far=data['far'])
        render_test_dl = DataLoader(dataset=render_test_data,
                                    batch_size=args.batch_size,
                                    pin_memory=pin_mem)

    # Custom data for full rendering (poses only)
    render_custom_shape = None
    if args.render_custom_poses > 0:
        height, width = data['test']['images'].shape[1:3]
        radius = meta['Acquisition']['Object to Source (mm)']
        custom_poses = build_spherical_render_poses(radius=radius)
        render_custom_shape = (len(custom_poses), height, width, 1)
        render_custom_data = PosesDataset(poses=custom_poses, height=height,
                                          width=width,
                                          focal_length=data['focal_length'],
                                          near=data['near'], far=data['far'])
        render_custom_dl = DataLoader(dataset=render_custom_data,
                                      batch_size=args.batch_size,
                                      pin_memory=pin_mem)
        custom_fps = render_custom_shape[0] / 5 if render_custom_shape[
                                                       0] < 50 else 30

    # Report loaded datasets
    data_msg = 'Dataset info:\n' \
               f'\t- Training images: {train_data.num_images} {train_data.image_dims}\n' \
               f'\t- Testing images: {test_data.num_images} {test_data.image_dims}'
    if log_data is not None:
        data_msg += f'\n\t- Log images: {log_shape[0]} {log_shape[1:]}'
    if render_custom_shape is not None:
        data_msg += f'\n\t- Custom render images: ' \
                    f'{render_custom_shape[0]} {render_custom_shape[1:]}'
    logger.info(data_msg)

    # Setup sampler
    ray_sampler = LinearRaySampler(num_samples=args.samples)
    train_data.ray_sampler = test_data.ray_sampler = ray_sampler
    if log_data is not None:
        log_data.ray_sampler = ray_sampler

    # Setup tensorboard logging
    writer = SummaryWriter(log_dir=str(tensorboard_dir))
    if train_data is not None:
        with torch.no_grad():
            _, pts = next(iter(train_dl))
            writer.add_graph(model, pts.to(device))

    # Add expected test images to Tensorboard
    if test_data.images.shape[0] < 50:
        test_fps = test_data.images.shape[0] / 5
    else:
        test_fps = 30
    expected_frames = np.expand_dims(np.moveaxis(test_data.images, 3, 1), 0)
    writer.add_video(tag='Renders/expected',
                     vid_tensor=torch.from_numpy(expected_frames),
                     global_step=0,
                     fps=test_fps)
    if log_data is not None:
        writer.add_images(tag='Projections/expected',
                          img_tensor=data['log']['images'],
                          global_step=0,
                          dataformats='NHWC')

    # Initialize to a starting value
    if args.init_val is not None:
        logger.info('Initializing to uniform value')
        init_val = torch.full((args.batch_size * args.samples, 1),
                              args.init_val,
                              dtype=torch.float32,
                              device=device)
        loss = args.init_threshold + 1
        iter_idx = 0
        while loss > args.init_threshold:
            pts = random_volume_points(data['volume']['bounds'],
                                       count=args.batch_size * args.samples)
            optimizer.zero_grad(set_to_none=True)
            loss = render_voxels(pts=torch.from_numpy(pts).to(device),
                                 model=model,
                                 expected=init_val.to(device),
                                 loss_fn=loss_fn)["loss"]
            loss.backward()
            optimizer.step()
            if iter_idx % 100 == 0:
                logger.info(
                    f'Initialization iteration {iter_idx}, Loss: {loss}')
            iter_idx += 1
        logger.info(f'Initialization iteration {iter_idx}, Loss: {loss}')

        # reset the optimizer
        for g in optimizer.param_groups:
            g['lr'] = args.learning_rate

    # Variables for tracking info about the training session
    avg_batch_duration = 0
    avg_epoch_duration = 0
    msg_prefix = ''
    last_valid_checkpoint = 'None'
    total_batches = len(train_dl) * args.epochs
    if args.batches is not None:
        total_batches = min(total_batches, args.batches)
    total_batch_digits = len(str(total_batches))
    epoch_log_interval = 10
    batch_log_interval = 10
    interrupted = False

    # Iterate
    logger.info('Begin training')
    try:
        for epoch in range(args.epochs):
            epoch_start = dt.now(tz.utc)
            for batch_idx, (pixels, pts) in enumerate(train_dl):
                # Calculate global batch idx
                all_batch_idx = epoch * len(train_dl) + batch_idx
                if args.batches is not None and all_batch_idx == args.batches:
                    raise MaxBatchReached()
                msg_prefix = f'[{all_batch_idx:>{total_batch_digits}d}/' \
                             f'{total_batches:<{total_batch_digits}d}] '
                batch_start = dt.now(tz.utc)

                # EVALUATE TRAINING DATA #
                optimizer.zero_grad(set_to_none=True)
                loss = render_rays(pts=pts.to(device), model=model,
                                   expected=pixels.to(device),
                                   loss_fn=loss_fn)["loss"]

                # Backprop
                loss.backward()
                optimizer.step()

                # Calculate batch duration
                batch_duration = (dt.now(tz.utc) - batch_start).total_seconds()
                avg_batch_duration = avg_batch_duration + (
                        batch_duration - avg_batch_duration) / (
                                             all_batch_idx + 1)

                # Checks for non-parameterized logging
                need_epoch_log = epoch % epoch_log_interval == 0 and batch_idx == 0
                need_batch_log = all_batch_idx % batch_log_interval == 0

                # Print training summary to console/log on interval
                if all_batch_idx < 10 or need_epoch_log or need_batch_log:
                    logger.info(msg_prefix +
                                f'Epoch: {epoch:>5d}/{args.epochs:<5d} '
                                f'Batch: {batch_idx:>5d}/{len(train_dl):<5d} '
                                f'Loss: {loss:>7f} ({batch_duration:5.3g}s)')

                # Write training summary to tensorboard
                if all_batch_idx % args.tb_train_summary == 0:
                    writer.add_scalar(tag='Loss/train',
                                      scalar_value=loss,
                                      global_step=all_batch_idx)

                # Write model checkpoint
                if need_epoch_log or need_batch_log:
                    logger.debug(msg_prefix +
                                 f'Saving model checkpoint: {all_batch_idx}')
                    chkpt_file = f'checkpoint_e{epoch}_b{batch_idx}.tar'
                    save_checkpoint(path=model_dir / chkpt_file,
                                    epoch=epoch,
                                    batch=batch_idx,
                                    model=model,
                                    optimizer=optimizer)
                    last_valid_checkpoint = chkpt_file

                # EVALUATE TEST DATA #
                if all_batch_idx % args.tb_test_summary == 0:
                    test_start = dt.now(tz.utc)
                    test_loss = evaluate_test_batch(dataloader=test_dl,
                                                    model=model,
                                                    loss_fn=loss_fn,
                                                    device=device)["loss"]
                    test_duration = (
                            dt.now(tz.utc) - test_start).total_seconds()

                    # Write training summary to tensorboard
                    writer.add_scalar('Loss/test', test_loss, all_batch_idx)

                    # Report to console only when we also reported batch loss
                    if all_batch_idx < 10 or need_epoch_log or need_batch_log:
                        logger.info(msg_prefix +
                                    f'Evaluated test set. '
                                    f'Loss: {test_loss:>7f} '
                                    f'({test_duration:5.3g}s)')

                # Reconstruct logging images and add to tb
                if log_data is not None and all_batch_idx % args.tb_test_image == 0:
                    render_start = dt.now(tz.utc)
                    images, _ = render_images(dataloader=log_dl,
                                              shape=log_shape,
                                              model=model,
                                              buffer_max=buffer_max,
                                              device=device)
                    render_duration = (
                            dt.now(tz.utc) - render_start).total_seconds()
                    logger.info(msg_prefix +
                                f'Rendered log test projection images '
                                f'({render_duration:5.3g}s)')
                    images = np.expand_dims(images, 3)
                    writer.add_images(tag='Projections/test',
                                      img_tensor=images,
                                      global_step=all_batch_idx,
                                      dataformats='NHWC')

                # Reconstruct slice images and add to tb / write to path
                can_do_slice = slices_data is not None
                need_tb_slice = all_batch_idx % args.tb_test_image == 0
                need_render_slice = args.render_slices > 0 and all_batch_idx % args.render_slices == 0
                if can_do_slice and (need_tb_slice or need_render_slice):
                    # setup image file if we need to save slices to disk
                    render_path = None
                    if need_render_slice:
                        render_file = f'render_slices_e{epoch}_b{batch_idx}.tif'
                        render_path = renders_dir / render_file

                    render_start = dt.now(tz.utc)
                    images, _ = render_images(dataloader=slices_dl,
                                              shape=slices_shape,
                                              model=model,
                                              render_fn=render_voxels,
                                              buffer_max=buffer_max,
                                              flush_path=render_path,
                                              device=device)
                    render_duration = (
                            dt.now(tz.utc) - render_start).total_seconds()
                    logger.info(msg_prefix +
                                f'Rendered log test slice images '
                                f'({render_duration:5.3g}s)')
                    images = np.expand_dims(images, 3)

                    # Add slice to tensorboard as needed
                    if need_tb_slice:
                        writer.add_images(tag='Slices/test',
                                          img_tensor=images,
                                          global_step=all_batch_idx,
                                          dataformats='NHWC')

                # Reconstruct all testing images
                need_test_render = all_batch_idx > 0 and \
                                   args.render_test_poses > 0 and \
                                   all_batch_idx % args.render_test_poses == 0
                if need_test_render:
                    # Render images
                    logger.info(f'{msg_prefix}'
                                f'Starting render of full test image set...')
                    render_file = f'render_test_e{epoch}_b{batch_idx}.tif'
                    render_path = renders_dir / render_file

                    render_start = dt.now(tz.utc)
                    images, buffer = render_images(dataloader=render_test_dl,
                                                   device=device, model=model,
                                                   shape=render_test_shape,
                                                   buffer_max=buffer_max,
                                                   flush_path=render_path)
                    render_duration = (
                            dt.now(tz.utc) - render_start).total_seconds()

                    # Report
                    logger.info(msg_prefix +
                                f'Rendered full test images '
                                f'({render_duration:5.3f}s)')
                    images = np.expand_dims(images, (0, 2))
                    writer.add_video(tag='Renders/test',
                                     vid_tensor=torch.from_numpy(
                                         images).detach(),
                                     global_step=all_batch_idx,
                                     fps=test_fps)

                # Reconstruct custom poses
                need_custom_render = all_batch_idx > 0 and \
                                     args.render_custom_poses > 0 and \
                                     all_batch_idx % args.render_custom_poses == 0
                if need_custom_render:
                    # Render images
                    logger.info(msg_prefix +
                                'Starting render of custom image set...')
                    render_path = renders_dir / f'render_custom_e{epoch}_b{batch_idx}.tif'

                    render_start = dt.now(tz.utc)
                    images, buffer = render_images(dataloader=render_custom_dl,
                                                   device=device, model=model,
                                                   shape=render_custom_shape,
                                                   buffer_max=buffer_max,
                                                   flush_path=render_path)
                    render_duration = (
                            dt.now(tz.utc) - render_start).total_seconds()

                    # Report
                    logger.info(msg_prefix +
                                f'Rendered custom images '
                                f'({render_duration:5.3f}s)')
                    images = np.expand_dims(images, (0, 2))
                    writer.add_video(tag='Renders/custom',
                                     vid_tensor=torch.from_numpy(
                                         images).detach(),
                                     global_step=all_batch_idx,
                                     fps=custom_fps)

                # Increase logging interval by power of 10
                if all_batch_idx == batch_log_interval * 10:
                    batch_log_interval = batch_log_interval * 10

            # Track epoch duration
            epoch_duration = (dt.now(tz.utc) - epoch_start).total_seconds()
            avg_epoch_duration = avg_epoch_duration + (
                    epoch_duration - avg_epoch_duration) / (epoch + 1)

            # Print epoch times
            if epoch % epoch_log_interval == 0:
                logger.info(msg_prefix +
                            f'Epoch {epoch} complete ({epoch_duration:5.3g}s)')

            # Increase logging interval by power of 10
            if epoch == epoch_log_interval * 10:
                epoch_log_interval = epoch_log_interval * 10

    except KeyboardInterrupt:
        logger.warning(f'Training interrupted.')
        interrupted = True
    except MaxBatchReached:
        logger.info(f'Training stopped. Max batch reached.')
    else:
        logger.info(f'Training complete.')

    # Log info
    logger.info(f'Training stats:\n'
                f'\tLast epoch: {epoch}\n'
                f'\tLast batch: {batch_idx}\n'
                f'\tAvg. batch duration: {avg_batch_duration:.5f}s\n'
                f'\tAvg. epoch duration: {avg_epoch_duration:.5f}s\n'
                f'\tLast intermediate checkpoint: {last_valid_checkpoint}')

    # Write final model
    if not interrupted:
        logger.info(f'Saving final model checkpoint')
        chkpt_path = model_dir / f'checkpoint_final.tar'
        save_checkpoint(path=chkpt_path,
                        epoch=epoch,
                        batch=batch_idx,
                        model=model,
                        optimizer=optimizer)

        # Render test images
        if args.render_test_poses > 0:
            logger.info('Starting final render of full test image set...')
            render_path = renders_dir / f'render_test_final.tif'

            render_start = dt.now(tz.utc)
            images, buffer = render_images(dataloader=render_test_dl,
                                           device=device,
                                           model=model,
                                           shape=render_test_shape,
                                           buffer_max=buffer_max,
                                           flush_path=render_path)
            render_duration = (dt.now(tz.utc) - render_start).total_seconds()

            # Report
            logger.info(f'Rendered full test images ({render_duration:5.3f}s)')
            images = np.expand_dims(images, (0, 2))
            writer.add_video(tag='Renders/test',
                             vid_tensor=torch.from_numpy(images).detach(),
                             global_step=all_batch_idx,
                             fps=test_fps)

        # Render custom images
        if args.render_custom_poses > 0:
            # Render images
            logger.info('Starting final render of custom image set...')
            render_path = renders_dir / f'render_custom_final.tif'

            render_start = dt.now(tz.utc)
            images, buffer = render_images(dataloader=render_custom_dl,
                                           device=device,
                                           model=model,
                                           shape=render_custom_shape,
                                           buffer_max=buffer_max,
                                           flush_path=render_path)
            render_duration = (dt.now(
                tz.utc) - render_start).total_seconds()

            # Report
            logger.info(f'Rendered custom images ({render_duration:5.3f}s)')
            images = np.expand_dims(images, (0, 2))
            writer.add_video('Renders/custom',
                             torch.from_numpy(images).detach(), all_batch_idx,
                             fps=custom_fps)


if __name__ == '__main__':
    logger = logging.getLogger('leaf.train')
    logger.error('This program has been deprecated. Use pyleaf-recon instead.')
    sys.exit(1)
