import json
import logging
from pathlib import Path

import imageio
import numpy as np
from skimage.transform import rescale
from skimage.util import img_as_float

from leaf.ray_tracing import ConeBeamProjector, normalize, rotate


def pyleaf_dataset_volume_size(meta):
    w = meta['detector']['cols']
    h = meta['detector']['rows']
    sso = meta['source-to-center']
    ssd = meta['source-to-detector']
    pix_size = meta['detector']['pixel-size'] * sso / ssd / 1000.0
    vol_radius = w * pix_size / 2.
    vol_half_h = h * pix_size / 2.
    min_pt = np.array([-vol_radius, -vol_radius, -vol_half_h], dtype=np.float32)
    max_pt = np.array([vol_radius, vol_radius, vol_half_h], dtype=np.float32)
    return min_pt, max_pt


def load_pyleaf_dataset(data_dir, offset=0, skip=1, scale=None,
                        load_images=True, single_slice=False, **kwargs):
    logger = logging.getLogger(__name__)

    logs = list(Path(data_dir).glob('*.json'))
    if len(logs) == 0:
        raise RuntimeError('No PyLeaf JSON file found.')
    elif len(logs) > 1:
        logger.warning(f'More than one JSON file in data directory.')

    logger.debug(f'Loading PyLeaf dataset: {logs[0]}')

    with Path(logs[0]).open('r') as fp:
        meta = json.load(fp)

    # Get acquisition geometry parameters
    source_to_sample = meta['source-to-center']
    source_to_detector = meta['source-to-detector']
    pix_size = meta['detector']['pixel-size'] / 1000.

    # Scale the sensor pixel size
    if scale is not None:
        pix_size /= scale

    # Load the poses and the images (optional)
    projectors = []
    for proj in meta['projections'][offset:None:skip]:
        # Calculate source position
        up = (0, 0, 1)
        src = np.array((0, -source_to_sample, 0), dtype=np.float32)
        theta = np.deg2rad(proj['gantry-angle'])
        src = rotate(theta, up) @ src

        # Get center of projection plane
        n = normalize(-src)
        lookat = src + n * source_to_detector

        # Load images if requested
        img = None
        size = None
        if load_images:
            fname = Path(data_dir) / proj['file-name']
            img = np.array(imageio.imread(fname))
            if scale is not None:
                img = rescale(img, scale=scale, anti_aliasing=True)
            else:
                img = img_as_float(img)

            # Scale to [0, 1] and invert
            img = 1. - (img / 255.)

            # Crop to middle row(s)
            if single_slice:
                start = (img.shape[0] - 1) // 2
                if start % 2 == 0:
                    img = img[start:start + 2]
                else:
                    img = img[start:start + 1]

            # Reshape image
            s = (img.shape[0], img.shape[1], 1)
            img = img.reshape(s).astype(np.float32)
        else:
            width = meta['detector']['cols']
            height = meta['detector']['rows']
            if scale is not None:
                width = np.round(width * scale).astype(int)
                height = np.round(height * scale).astype(int)
            size = (height, width)

        # Add projector
        cam = ConeBeamProjector(pos=src, lookat=lookat, up=up, image=img,
                                size=size, pixel_size=pix_size)
        projectors.append(cam)

    # Calculate volume size
    vol_min, vol_max = pyleaf_dataset_volume_size(meta)

    # Scaled pixel size (mm)
    slice_pix_size = meta['detector']['pixel-size']
    slice_pix_size *= source_to_sample / source_to_detector / 1000.0

    return {
        'projectors': projectors,
        'volume': {
            'min': vol_min,
            'max': vol_max,
            'diameter': np.amax((vol_max - vol_min)[0:2]),
            'height': (vol_max - vol_min)[2]
        },
        'slices': {
            'cols': meta['detector']['cols'],
            'rows': meta['detector']['cols'],
            'pix-size': slice_pix_size
        },
        'metadata': meta
    }
