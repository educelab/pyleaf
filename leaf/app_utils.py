import logging
from pathlib import Path

import torch


def write_config(config: Path, args):
    logger = logging.getLogger(__name__)
    logger.debug(f'Writing config file: {config}')
    with config.open(mode='w') as file:
        for arg in vars(args):
            attr = getattr(args, arg)
            if attr is None:
                continue
            arg = arg.replace('_', '-')
            file.write(f'{arg} = {attr}\n')


def report_cuda_usage(device, logger):
    logger.info(f'PyTorch device: {device}')
    if device.type == 'cuda':
        logger.info(f'\t{torch.cuda.get_device_name(0)}')
        logger.info(
            f'\tMemory Allocated: {round(torch.cuda.memory_allocated(0) / 1024 ** 3, 1)} GB')
        logger.info(
            f'\tMemory Cached:    {round(torch.cuda.memory_reserved(0) / 1024 ** 3, 1)} GB')
