#!/bin/bash

#SBATCH -A gol_seales_uksr
#SBATCH --gres=gpu:1
#SBATCH --mail-type=END
#SBATCH --job-name=pyleaf
#SBATCH --output=pyleaf_%A_%a.out.txt

module load ccs/singularity

# Make sure overlay files exist
overlay="pyleaf.overlay"
if ! test -f "${overlay}" ; then
  echo "Creating ${overlay}"
  dd if=/dev/zero of="${overlay}" bs=1M count=500 && mkfs.ext3 -F "${overlay}"
fi

time singularity run --nv --overlay pyleaf.overlay ${PROJECT}/seales_uksr/containers/pyleaf.sif "$@"
