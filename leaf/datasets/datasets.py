import re
from enum import Enum, auto
from pathlib import Path

from leaf.datasets.internal import load_pyleaf_dataset
from leaf.datasets.phantoms import load_phantom_dataset
from leaf.datasets.skyscan import load_skyscan_dataset


class DatasetType(Enum):
    PYLEAF = auto()
    SKYSCAN = auto()
    PHANTOM = auto()
    UNKNOWN = auto()


def _get_dataset_type(data_dir) -> DatasetType:
    # Check for phantom prefix
    if re.search(r'^phantom://.*', data_dir):
        return DatasetType.PHANTOM

    # Check for PyLeaf JSON files
    files = list(Path(data_dir).glob('*.json'))
    if len(files) > 0:
        return DatasetType.PYLEAF

    # Check for Skyscan log files
    files = list(Path(data_dir).glob('*.log'))
    if len(files) > 0:
        return DatasetType.SKYSCAN

    # Unrecognized type
    return DatasetType.UNKNOWN


def load_dataset(data_dir, **kwargs):
    data_type = _get_dataset_type(data_dir)
    if data_type is DatasetType.PYLEAF:
        return load_pyleaf_dataset(data_dir=data_dir, **kwargs)
    elif data_type is DatasetType.SKYSCAN:
        return load_skyscan_dataset(data_dir=data_dir, **kwargs)
    elif data_type is DatasetType.PHANTOM:
        return load_phantom_dataset(data_dir=data_dir, **kwargs)
    elif data_type is DatasetType.UNKNOWN:
        raise RuntimeError(f'Unrecognized dataset type: {data_dir}')
