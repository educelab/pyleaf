import logging
from typing import Dict, List

import numpy as np
import torch
from scipy import ndimage
from torch.utils.data import Dataset

from leaf.pose_helpers import get_rays, spherical_pose
from leaf.rendering import LinearRaySampler, RaySampler


def permute(x, permutation=None):
    if permutation is None:
        permutation = torch.randperm(len(x)).tolist()
    return type(x)(x[i] for i in permutation)


def min_max_scale(x, min_x, max_x, a, b):
    return a + (x - min_x) * (b - a) / (max_x - min_x)


class MinMaxScaler:
    def __init__(self, in_bounds, out_bounds):
        """
        @param in_bounds: array of shape [[min_0, max_0], [min_1, max_1], ..., [min_n, max_n]]
        @param out_bounds: array of shape [[min_0, max_0], [min_1, max_1], ..., [min_n, max_n]]
        """
        self._in_bounds = np.array(in_bounds, dtype=np.float32)
        self._out_bounds = np.array(out_bounds, dtype=np.float32)

    def transform(self, coord):
        def apply_scale_row(c):
            return np.array(
                [min_max_scale(x, min_x, max_x, a, b) for
                 x, (min_x, max_x), (a, b)
                 in zip(c, self._in_bounds, self._out_bounds)],
                dtype=np.float32)

        coord = np.array(coord)
        if coord.ndim == 1:
            return apply_scale_row(coord)
        elif coord.ndim == 2:
            return np.apply_along_axis(apply_scale_row, axis=1, arr=coord)
        else:
            raise ValueError('Too many dimensions')

    @property
    def in_bounds(self):
        return self._in_bounds

    @property
    def out_bounds(self):
        return self._out_bounds


class ProjectorDataset(Dataset):
    def __init__(self, projectors, ray_sampler: RaySampler = None):
        super(ProjectorDataset, self).__init__()

        # Store the projectors
        self._projs = projectors
        if ray_sampler is None:
            self._sampler = LinearRaySampler()
        else:
            self._sampler = ray_sampler

        # num pixel samples
        self._num_pixels = 0
        for p in self._projs:
            self._num_pixels += p.num_pixels

    def __getitem__(self, idx):
        # Range check
        if not (0 <= idx < self._num_pixels):
            raise IndexError('list index out of range')

        # Get the ray for this pixel
        proj_start = 0
        ray = None
        for proj in self._projs:
            if (proj_start + proj.num_pixels) > idx:
                ray = proj.ray_index(idx - proj_start)
                break
            else:
                proj_start += proj.num_pixels

        # Sample the ray
        o, d, dist, val = ray
        samples = self._sampler.sample(origins=np.expand_dims(o, 0),
                                       directions=np.expand_dims(d, 0),
                                       near=1.e-7, far=dist)

        return np.squeeze(samples).astype(np.float32), val

    def __len__(self):
        return self._num_pixels

    @property
    def ray_sampler(self) -> RaySampler:
        return self._sampler

    @ray_sampler.setter
    def ray_sampler(self, sampler: RaySampler):
        self._sampler = sampler


class PosesDataset(Dataset):
    def __init__(self, poses, height, width, focal_length, near, far,
                 ray_sampler: RaySampler = None):
        if ray_sampler is None:
            self._sampler = LinearRaySampler()
        else:
            self._sampler = ray_sampler

        # Setup rays
        rays_origin = []
        rays_dir = []
        for idx, p in enumerate(poses):
            orig, direction = get_rays(height=height, width=width,
                                       focal=focal_length, c2w=p)
            rays_origin.append(orig)
            rays_dir.append(direction)

        rays_origin = np.array(rays_origin)
        self._rays_orig = rays_origin.reshape(-1, rays_origin.shape[-1])

        rays_dir = np.array(rays_dir)
        self._rays_dir = rays_dir.reshape(-1, rays_dir.shape[-1])

        self._fl = focal_length
        self._near = near
        self._far = far

    def __getitem__(self, idx):
        # Get ray samples
        origin = np.expand_dims(self._rays_orig[idx, :], 0)
        direction = np.expand_dims(self._rays_dir[idx, :], 0)
        samples = self._sampler.sample(origins=origin, directions=direction,
                                       near=self._near, far=self._far)

        return np.squeeze(samples)

    def __len__(self):
        return self._rays_orig.shape[0]

    @property
    def origins(self):
        return self._rays_orig

    @property
    def directions(self):
        return self._rays_dir

    @property
    def focal_length(self):
        return self._fl

    @property
    def bounds(self):
        return self._near, self._far

    @property
    def near(self):
        return self._near

    @property
    def far(self):
        return self._far

    @property
    def ray_sampler(self) -> RaySampler:
        return self._sampler

    @ray_sampler.setter
    def ray_sampler(self, sampler: RaySampler):
        self._sampler = sampler


class SlicesDataset(Dataset):
    def __init__(self, centers: np.ndarray, extents: np.ndarray, height: int,
                 width: int):
        """
        @param centers: Center position of each slice in volume space
               (shape: [n, x, y, z])
        @param extents: Extent of slice plane in volume units
               (shape: [n, width, height])
        @param height: Height of slice image in pixels
        @param width: Width of slice image in pixels
        """
        self._centers = centers
        self._extents = extents
        self._height = height
        self._width = width

    def __getitem__(self, idx):
        slice_idx = idx // (self.width * self.height)
        slice_start = slice_idx * self.width * self.height
        pixel_idx = idx - slice_start
        v = pixel_idx // self.height
        u = pixel_idx - (v * self.height)

        center = self._centers[slice_idx]
        vol_width = self._extents[slice_idx][0]
        vol_height = self._extents[slice_idx][1]

        delta_x = vol_width / self.width
        delta_y = vol_height / self.height
        x = center[0] - (vol_width / 2) + (u + 0.5) * delta_x
        # TODO: Y seems flipped but the rendered output matches the FBP slice?
        y = center[1] - (vol_height / 2) + (v + 0.5) * delta_y
        z = center[2]

        return np.array([x, y, z], dtype=np.float32)

    def __len__(self):
        return self._centers.shape[0] * self._height * self._width

    @property
    def centers(self):
        return self._centers

    @property
    def extents(self):
        return self._extents

    @property
    def width(self):
        return self._width

    @property
    def height(self):
        return self._height


class ImageDataset(Dataset):
    def __init__(self, image: np.ndarray, transform=None):
        """
        @param image: Image data
               (shape: [n, h, w, c])
        """
        self._image = image
        self._tfm = transform

    def __getitem__(self, idx):
        z, y, x = coord = np.unravel_index(idx, self._image.shape[:3])
        if self._tfm is not None:
            x, y, z = self._tfm.transform((x, y, z))
        return np.array((x, y, z), dtype=np.float32), self._image[coord]

    def __len__(self):
        return np.prod(self._image.shape[0:3])


class MultiResImageDataset(Dataset):
    def __init__(self,
                 image: np.ndarray,
                 scales: List[float],
                 crops: Dict):
        """
        @param image: Original image
               (shape: [n, h, w, c])
        @param scales: For each image in images, the scale factor that maps the
               unscaled image to the scaled image
        @param crops: A dictionary describing how to crop a scaled image. Each
               key is the integer index of a scale factor in the scales list.
               The associated value has the following structure:
               {
                 'index': Same as idx
                 'n': Size of the cropped N axis (int, None),
                 'h': Size of the cropped H axis (int, None),
                 'w': Size of the cropped W axis (int, None),
                 'z': Z position of the cropped image origin (int, None),
                 'y': Y position of the cropped image origin (int, None),
                 'x': X position of the cropped image origin (int, None),
               }
        """
        logger = logging.getLogger(__name__)
        self._images = []
        self._origins = []
        self._scales = []
        self._num_pixels = 0

        # Construct scaled images
        for idx, scale in enumerate(scales):
            # Duplicate the image
            sub_img = image.copy()
            # If this image has crop settings...
            origin = (0, 0, 0)
            if idx in crops.keys():
                # Get the parameters
                _, n, h, w, z, y, x = crops[idx].values()
                # Adjust n,h,w for np indexing
                if n is not None and z is not None:
                    n += z
                if h is not None and y is not None:
                    h += y
                if w is not None and x is not None:
                    w += x
                # Crop
                sub_img = sub_img[z:n, y:h, x:w]
                z = 0 if z is None else z
                y = 0 if y is None else y
                x = 0 if x is None else x
                origin = (z, y, x)

            # Scale the image after cropping
            if scale != 1.:
                # For each spatial dimension, make sure we don't scale to zero
                zoom = []
                for d in sub_img.shape[:3]:
                    if d * scale < 1:
                        zoom.append(1)
                    else:
                        zoom.append(scale)
                # Don't scale the color channels
                zoom.append(1)
                sub_img = ndimage.zoom(sub_img, zoom=zoom,
                                       prefilter=(scale < 1.))
            if sub_img.size == 0:
                logger.warning(f'Image with scale {scale}x is empty. Skipping.')
                continue

            # Add to list of scaled images
            self._images.append(sub_img)
            self._origins.append(origin)
            self._scales.append(scale)
            self._num_pixels += np.prod(sub_img.shape[:3])

    def __getitem__(self, idx):
        # Range check
        if not (0 <= idx < self._num_pixels):
            raise IndexError('list index out of range')

        # Find the image this index belongs to
        img_start = 0
        image = None
        image_index = None
        for image_index, image in enumerate(self._images):
            img_pixels = np.prod(image.shape[:3])
            if (img_start + img_pixels) > idx:
                break
            else:
                img_start += img_pixels

        # Get pixel coordinate and intensity from scaled image
        z, y, x = coord = np.unravel_index(idx - img_start, image.shape[:3])
        val = image[coord]

        # Shift and scale 3D coordinates into the space of the original image
        scale = self._scales[image_index]
        oz, oy, ox = self._origins[image_index]
        x = ox + (x / scale)
        y = oy + (y / scale)
        z = oz + (z / scale)

        return np.array((x, y, z), dtype=np.float32), val

    def __len__(self):
        return self._num_pixels


class ImageCoordinateDataset(Dataset):
    def __init__(self, shape, origin=(0., 0., 0.), stride=1.0):
        """
        @param Shape: Image data
               (shape: [n, h, w, c])
        """
        self._shape = shape
        self._origin = np.array(origin, dtype=np.float32)
        self._stride = stride

    def __getitem__(self, idx):
        coord = np.unravel_index(idx, self._shape[:3])
        coord = self._origin + np.array(coord) * self._stride
        return np.flip(coord).astype(dtype=np.float32)

    def __len__(self):
        return np.prod(self._shape[0:3])


def build_spherical_render_poses(theta_start: float = -180,
                                 theta_end: float = 180, theta_steps: int = 40,
                                 phi_start: float = -30, phi_end: float = -30,
                                 phi_steps: int = 1, radius: float = 0.5):
    poses = []
    for phi in np.linspace(phi_start, phi_end, phi_steps, endpoint=False):
        for theta in np.linspace(theta_start, theta_end, theta_steps,
                                 endpoint=False):
            poses.append(spherical_pose(theta, phi, radius))

    return np.stack(poses, 0)
