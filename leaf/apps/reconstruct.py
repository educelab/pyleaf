import logging
import random
import sys
from collections import OrderedDict
from datetime import datetime as dt, timezone as tz
from distutils.util import strtobool
from pathlib import Path

import bitmath
import configargparse
import numpy as np
import torch
import torchsummary
from bitmath import Byte
from torch.nn import ReLU, Sequential
from torch.optim.lr_scheduler import LinearLR
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter

import leaf.app_utils as app_utils
import leaf.log_utils as log_utils
from leaf.data import ProjectorDataset, SlicesDataset
from leaf.datasets import load_dataset
from leaf.embedding import GaussianEmbedding, MinMaxScalingEmbedder, \
    PassThruEmbedder, PositionalEncoder
from leaf.models import Leaf, save_checkpoint
from leaf.modules import Sin
from leaf.rendering import LinearRaySampler, render_images, render_rays, \
    render_voxels


class MaxBatchReached(Exception):
    """Indicates that the user-defined batch maximum has been reached"""


def main():
    # Parse arguments
    parser = configargparse.ArgumentParser(prog='pyleaf-recon')

    # Generic options
    parser.add_argument('-c', '--config', is_config_file=True,
                        help='Config file path')
    parser.add_argument('-i', '--input', type=str,
                        default='phantom://shepp-logan-modified',
                        help='Path to input dataset. To use a built-in '
                             'phantom, use the phantom:// prefix (e.g. '
                             '-i phantom://shepp-logan-modified)')
    parser.add_argument('-o', '--output', type=str,
                        help='Path to output directory for results. If not '
                             'specified, a results directory will be '
                             'created in the current working directory')
    parser.add_argument('-n', '--name', type=str, help='Experiment name.')
    parser.add_argument('--log', type=str,
                        help='Write experiment log to a specific file. If not '
                             'specified, log will be written to output '
                             'directory')

    # Dataset options
    dataset_args = parser.add_argument_group('dataset options')
    dataset_args.add_argument('--scale', type=float,
                              help='Scale dataset images by the given factor')
    dataset_args.add_argument('--offset', type=int, default=0,
                              help='When loading a training dataset, ignore '
                                   'the first --train-offset images')
    dataset_args.add_argument('--skip', type=int, default=1,
                              help='When loading a training dataset, only '
                                   'include every --train-skip images')
    dataset_args.add_argument('--permute', action='store_true', default=False,
                              help='If enabled, permute the projection images')
    dataset_args.add_argument('--single-slice', action='store_true',
                              default=False,
                              help='If enabled, only load the middle 1 or 2 '
                                   'rows of the projection')

    # Phantom options
    phantom_args_desc = 'configuration options for built-in phantom:// datasets'
    phantom_args = parser.add_argument_group(title='phantom options',
                                             description=phantom_args_desc)
    phantom_args.add_argument('--phantom-dim', type=int, default=192,
                              help='Edge length of the phantom in pixels')
    phantom_args.add_argument('--phantom-range-start', type=float, default=0.,
                              help='Starting angle for generated projections, '
                                   'in degrees')
    phantom_args.add_argument('--phantom-range-end', type=float, default=360.,
                              help='Ending angle (non-inclusive) for generated '
                                   'projections, in degrees')
    phantom_args.add_argument('--phantom-range-count', type=int, default=360,
                              help='Number of projections sampled over the '
                                   'specified angular range')

    # Model options
    model_args = parser.add_argument_group('model options')
    model_args.add_argument('--model-checkpoint', type=int,
                            help='Save a model checkpoint every N batches. '
                                 'A checkpoint will always be saved at the '
                                 'beginning of every epoch.')
    model_args.add_argument('-d', '--depth', type=int, default=8,
                            help='Number of hidden layers')
    model_args.add_argument('-w', '--width', type=int, default=256,
                            help='Number of neurons in each hidden layer')

    # Activation options
    activation_args = parser.add_argument_group('activation options')
    activation_args.add_argument('--activation', type=str.lower, default='relu',
                                 choices=['relu', 'sin'],
                                 help='Hidden layer activation function')
    activation_args.add_argument('--sin-w0', type=float, default='30',
                                 help='When using Sin activation, set the '
                                      'value of the weights in the first layer')

    # Embedder options
    embedder_args = parser.add_argument_group('embedder options')
    embedder_args.add_argument('--embedder', type=str.lower, default='posenc',
                               choices=['none', 'minmax', 'posenc', 'gaussian'],
                               help='Point embedding function')
    embedder_args.add_argument('--posenc-freqs', type=int, default=10,
                               help='Number of frequency components in the '
                                    'posenc embedder')
    embedder_args.add_argument('--gaussian-freqs', type=int, default=256,
                               help='Number of frequency components generated '
                                    'by the gaussian embedder.')
    embedder_args.add_argument('--gaussian-scale', type=float, default=10.,
                               help='Std. deviation of the distribution used '
                                    'by the gaussian embedder')

    # Training options
    training_args = parser.add_argument_group('training options')
    training_args.add_argument('-e', '--epochs', type=int, default=1000,
                               help='Training epochs')
    training_args.add_argument('-b', '--batch-size', type=int, default=1024,
                               help='Mini-batch size')
    training_args.add_argument('--batches', type=int,
                               help='If specified, stop training after a '
                                    'maximum of N batches')
    training_args.add_argument('--shuffle', default=True,
                               type=lambda x: bool(strtobool(x)),
                               help='If enabled, shuffle the training samples')
    training_args.add_argument('--learning-rate', type=float, default=1.e-3,
                               help='Optimizer learning rate')
    training_args.add_argument('--scheduler-epochs', type=int,
                               help='When provided, decay the learning rate '
                                    'over N epochs')

    # Ray tracer options
    ray_tracer_args = parser.add_argument_group('ray tracer options')
    ray_tracer_args.add_argument('-s', '--samples', type=int, default=64,
                                 help='Number of ray samples')
    ray_tracer_args.add_argument('--jitter', default=True,
                                 type=lambda x: bool(strtobool(x)),
                                 help='If enabled, jitter the ray samples')

    # Tensorboard options
    tb_args = parser.add_argument_group('tensorboard options')
    tb_args.add_argument('--tb-train-summary', type=int, default=10,
                         help='Log training summary to Tensorboard every N '
                              'batches')

    # Rendering options
    render_args = parser.add_argument_group('rendering options')
    render_args.add_argument('--render-slices', type=int, default=5000,
                             help='Render the slice set every N batches. '
                                  'Disabled if N=0.')
    render_args.add_argument('--render-slice-z', action='extend', nargs='+',
                             type=float,
                             help='Z coordinate for a slice render. May be '
                                  'specified multiple times. If not specified, '
                                  'defaults to a full volume render. If '
                                  '--single-slice is enabled, defaults to 0.')
    render_args.add_argument('--buffer-size', type=str, default='100M',
                             help='Pixel buffer size. Rendered images will be '
                                  'written to disk when the pixel buffer is '
                                  'full or the rendering job is complete.')

    # Performance options
    perf_args = parser.add_argument_group('performance options')
    perf_args.add_argument('--data-workers', type=int, default=0,
                           help='Generate batches using N worker threads')

    args, unknown_args = parser.parse_known_args()

    # Setup experiment
    experiment_start = dt.now(tz.utc)
    datetime_str = experiment_start.strftime('%Y%m%d%H%M%S')
    if args.name is None:
        args.name = datetime_str + '_' + str(Path(args.input).stem)

    # Setup output directory
    if args.output is None:
        args.output = 'results/'
    experiment_output_dir = Path(args.output) / args.name
    log_dir = experiment_output_dir / 'logs'
    conf_dir = experiment_output_dir / 'configs'
    model_dir = experiment_output_dir / 'models'
    tensorboard_dir = experiment_output_dir / 'tensorboard' / datetime_str
    renders_dir = experiment_output_dir / 'renders' / datetime_str
    for d in [experiment_output_dir, log_dir, conf_dir, model_dir,
              tensorboard_dir, renders_dir]:
        d.mkdir(parents=True, exist_ok=True)

    # Setup logging
    if args.log is None:
        suffix = '_recon_log.txt'
        args.log = str(log_dir / (datetime_str + '_' + args.name + suffix))
    log_utils.setup_app_handlers(log_file=args.log)
    logger = logging.getLogger('leaf.recon')
    logger.info(f'Set up output directory: {experiment_output_dir}')

    # Setup buffer flush size
    try:
        buffer_max = np.floor(
            bitmath.parse_string_unsafe(args.buffer_size) / Byte(4)).astype(int)
    except ValueError as e:
        logger.error(e)
        sys.exit()
    logger.debug(
        f'Pixel buffer size: '
        f'{(buffer_max * Byte(4)).best_prefix().format("{value:.3f}{unit}")}, '
        f'length: {buffer_max} pixels')

    # Write config
    config = conf_dir / (datetime_str + '_' + args.name + '_recon_config.txt')
    args.config = str(config)
    app_utils.write_config(config, args)

    # Load dataset
    logger.info(f'Loading dataset: {args.input}')
    dataset = load_dataset(data_dir=args.input,
                           scale=args.scale,
                           offset=args.offset,
                           skip=args.skip,
                           single_slice=args.single_slice,
                           dim=args.phantom_dim,
                           theta_start=np.deg2rad(args.phantom_range_start),
                           theta_end=np.deg2rad(args.phantom_range_end),
                           num_projs=args.phantom_range_count)
    projs = dataset['projectors']
    vol_min, vol_max = dataset['volume']['min'], dataset['volume']['max']
    logger.info(f'Training projections: {len(projs)} {projs[0].proj_size}')

    # Permute the projections
    if args.permute:
        random.shuffle(projs)

    # Get device
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    pin_mem = device.type == 'cuda'

    # Setup activation function
    if args.activation == 'sin':
        activation = Sin
    else:
        activation = ReLU

    # Setup min-max embedder
    # Add 10% to volume size
    x_min = torch.tensor(vol_min)
    x_max = torch.tensor(vol_max)
    a = torch.full((3,), -np.pi)
    b = torch.full((3,), np.pi)
    minmax = MinMaxScalingEmbedder(x_min=x_min, x_max=x_max, a=a, b=b)

    # Setup full embedder
    if args.embedder == 'minmax':
        embedder = minmax
    elif args.embedder == 'posenc':
        posenc = PositionalEncoder(freqs=args.posenc_freqs)
        embedder = Sequential(OrderedDict([
            ('minmax', minmax),
            ('posenc', posenc)
        ]))
    elif args.embedder == 'gaussian':
        a = torch.full((3,), 0.)
        b = torch.full((3,), 2. * torch.pi)
        minmax = MinMaxScalingEmbedder(x_min=x_min, x_max=x_max, a=a, b=b)
        gauss = GaussianEmbedding(scale=args.gaussian_scale,
                                  output_dims=args.gaussian_freqs)
        embedder = Sequential(OrderedDict([
            ('minmax', minmax),
            ('gauss', gauss)
        ]))
    else:
        embedder = PassThruEmbedder()

    # Create model
    model = Leaf(hidden_layers=args.depth,
                 hidden_cns=args.width,
                 embedder=embedder,
                 activation_fn=activation,
                 sin_w0=args.sin_w0).to(device)

    # Setup optimizer
    optimizer = torch.optim.Adam(params=model.parameters(),
                                 lr=args.learning_rate)
    scheduler = None
    if args.scheduler_epochs is not None:
        scheduler = LinearLR(optimizer=optimizer,
                             total_iters=args.scheduler_epochs)

    # Setup loss
    loss_fn = torch.nn.MSELoss()

    # Preallocate model buffers
    pts = torch.rand((args.batch_size, args.samples, model.num_input_cns),
                     device=device)
    pixels = torch.rand((args.batch_size, 1), device=device)
    loss = render_rays(pts=pts, model=model, expected=pixels,
                       loss_fn=loss_fn)["loss"]
    loss.backward()
    model.zero_grad(set_to_none=True)

    # Report CUDA usage
    app_utils.report_cuda_usage(device, logger)

    # Report model summary
    input_size = (args.batch_size, args.samples, model.num_input_cns)
    summary = torchsummary.summary(model=model,
                                   input_data=input_size,
                                   branching=False,
                                   device=device,
                                   verbose=0)
    logger.info(f'Model summary (sizes represent a single training batch):\n'
                f'{summary}')

    # Setup ray sampler
    ray_sampler = LinearRaySampler(num_samples=args.samples,
                                   bounds=(vol_min, vol_max),
                                   jitter=args.jitter)

    # Setup training data
    train_data = ProjectorDataset(projs, ray_sampler)
    train = DataLoader(dataset=train_data,
                       batch_size=args.batch_size,
                       shuffle=args.shuffle,
                       num_workers=args.data_workers,
                       pin_memory=pin_mem)

    # Setup slices data
    width = dataset['slices']['cols']
    height = dataset['slices']['rows']
    vol_diam = dataset['volume']['diameter']

    if args.scale is not None:
        width = np.round(width * args.scale).astype(int)
        height = np.round(height * args.scale).astype(int)

    if args.render_slice_z is None:
        if args.single_slice:
            args.render_slice_z = [0.]
        else:
            voxel_size = dataset['slices']['pix-size']
            z_min, z_max = vol_min[2], vol_max[2]
            num_slices = int((z_max - z_min) // voxel_size)
            args.render_slice_z = np.linspace(z_min, z_max, num_slices)
        app_utils.write_config(config, args)  # Update the config file

    centers = []
    extents = []
    for z in args.render_slice_z:
        centers.append(np.array([0, 0, z], dtype=np.float32))
        extents.append(np.array([vol_diam, vol_diam], dtype=np.float32))
    centers = np.array(centers)
    extents = np.array(extents)
    slices_shape = (1, centers.shape[0], height, width)
    slices_data = SlicesDataset(centers=centers,
                                extents=extents,
                                height=height,
                                width=width)
    slices = DataLoader(dataset=slices_data,
                        batch_size=args.batch_size * args.samples,
                        pin_memory=pin_mem)

    # Write the graph model to Tensorboard
    writer = SummaryWriter(log_dir=str(tensorboard_dir))
    with torch.no_grad():
        pts, _ = next(iter(train))
        writer.add_graph(model, pts.to(device))

    # configuration
    total_batches = len(train) * args.epochs
    if args.batches is not None:
        total_batches = min(total_batches, args.batches)
    total_batch_digits = len(str(total_batches))
    epoch_log_interval = 10
    batch_log_interval = 10

    # Training
    try:
        logger.info('Begin training')
        for epoch in range(args.epochs):
            for batch_idx, (pts, pixels) in enumerate(train):
                # Calculate global batch idx
                all_batch_idx = epoch * len(train) + batch_idx
                if args.batches is not None and all_batch_idx == args.batches:
                    raise MaxBatchReached()
                msg_prefix = f'[{all_batch_idx:>{total_batch_digits}d}/' \
                             f'{total_batches:<{total_batch_digits}d}] '

                # Train on the batch
                optimizer.zero_grad(set_to_none=True)
                loss = render_rays(pts=pts.to(device),
                                   model=model,
                                   expected=pixels.to(device),
                                   loss_fn=loss_fn)["loss"]

                # Backprop
                loss.backward()
                optimizer.step()

                # Checks for non-parameterized logging
                is_first_batch = all_batch_idx == 0
                is_new_epoch = batch_idx == 0
                need_epoch_log = epoch % epoch_log_interval == 0 and is_new_epoch
                need_batch_log = all_batch_idx % batch_log_interval == 0

                # Print training summary to console/log on interval
                if all_batch_idx < 10 or need_epoch_log or need_batch_log:
                    logger.info(msg_prefix +
                                f'Epoch: {epoch:>5d}/{args.epochs:<5d} '
                                f'Batch: {batch_idx:>5d}/{len(train):<5d} '
                                f'Loss: {loss:>7f}')

                # Write training summary to tensorboard
                if all_batch_idx % args.tb_train_summary == 0:
                    writer.add_scalar(tag='Loss/train',
                                      scalar_value=loss,
                                      global_step=all_batch_idx)

                # Write model checkpoint
                need_epoch_ckpt = batch_idx == 0
                need_batch_ckpt = args.model_checkpoint is not None and \
                                  args.model_checkpoint > 0 and \
                                  all_batch_idx % args.model_checkpoint == 0
                if not is_first_batch and (need_epoch_ckpt or need_batch_ckpt):
                    logger.debug(msg_prefix +
                                 f'Saving model checkpoint: {all_batch_idx}')
                    chkpt_file = f'checkpoint_e{epoch}_b{batch_idx}.tar'
                    save_checkpoint(path=model_dir / chkpt_file,
                                    epoch=epoch,
                                    batch=batch_idx,
                                    model=model,
                                    optimizer=optimizer)
                    last_valid_checkpoint = chkpt_file

                # Reconstruct slice images and add to tb / write to path
                can_do_slice = slices_data is not None
                need_render_slice = args.render_slices > 0 and all_batch_idx % args.render_slices == 0
                if can_do_slice and need_render_slice:
                    render_file = f'render_slices_e{epoch}_b{batch_idx}.tif'
                    render_path = renders_dir / render_file

                    render_start = dt.now(tz.utc)
                    images, _ = render_images(dataloader=slices,
                                              shape=slices_shape,
                                              model=model,
                                              render_fn=render_voxels,
                                              buffer_max=buffer_max,
                                              flush_path=render_path,
                                              device=device)
                    render_duration = (
                            dt.now(tz.utc) - render_start).total_seconds()
                    logger.info(msg_prefix +
                                f'Rendered log test slice images '
                                f'({render_duration:5.3g}s)')
                    images = np.reshape(images,
                                        (centers.shape[0], height, width, 1))

                    # Add slice to tensorboard as needed
                    writer.add_images(tag='Slices/test',
                                      img_tensor=images,
                                      global_step=all_batch_idx,
                                      dataformats='NHWC')

            # Step if a scheduler is initialized
            if scheduler is not None:
                scheduler.step()
    except KeyboardInterrupt:
        logger.warning(f'Training interrupted.')
        interrupted = True
    except MaxBatchReached:
        logger.info(f'Training stopped. Max batch reached.')
    else:
        logger.info(f'Training complete.')


if __name__ == '__main__':
    main()
