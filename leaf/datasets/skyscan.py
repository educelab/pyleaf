import logging
import re
from pathlib import Path

import imageio
import numpy as np
from skimage.transform import rescale
from skimage.util import img_as_float
from tqdm import tqdm

from leaf.pose_helpers import spherical_pose
from leaf.ray_tracing import ConeBeamProjector, normalize, rotate

_REGEX_SECTION = re.compile(r'^\[(?P<sec>[\w\s]+)]$')
_REGEX_KEY_VALUE = re.compile(r'^(?P<key>.+)=(?P<value>.+)$')


def load_skyscan_metadata(basedir):
    logger = logging.getLogger(__name__)

    logs = list(Path(basedir).glob('*.log'))
    if len(logs) == 0:
        raise RuntimeError('No Skyscan log file found.')
    elif len(logs) > 1:
        logger.warning(f'More than one log file in data directory.')

    logger.debug(f'Loading Skyscan log: {logs[0]}')

    meta = {}
    section = None
    with Path(logs[0]).open('r') as log:
        for raw_line in log:
            line = raw_line.strip()

            # Parse the section breaks
            sec_match = _REGEX_SECTION.match(line)
            if sec_match:
                section_title = sec_match.group('sec')
                section = {}
                meta[section_title] = section
                continue

            # Parse the metadata lines
            kv_match = _REGEX_KEY_VALUE.match(line)
            if kv_match:
                key, value = kv_match.group('key', 'value')
                if value.strip().isdecimal():
                    value = int(value)
                else:
                    try:
                        value = float(value)
                    except ValueError:
                        pass
                section[key] = value
                continue

            # Didn't parse this line
            logger.warning(f'Line not parsed: {line}')

    return meta


def skyscan_volume_size(meta):
    w = meta['Acquisition']['Number of Columns']
    h = meta['Acquisition']['Number of Rows']
    pix_size = meta['Acquisition']['Image Pixel Size (um)'] / 1000.0
    vol_radius = w * pix_size / 2.
    vol_half_h = h * pix_size / 2.
    min_pt = np.array([-vol_radius, -vol_radius, -vol_half_h], dtype=np.float32)
    max_pt = np.array([vol_radius, vol_radius, vol_half_h], dtype=np.float32)
    return min_pt, max_pt


def load_skyscan_dataset(meta=None, data_dir=None, offset=0, skip=1, scale=None,
                         load_images=True, single_slice=False, **kwargs):
    if meta is None and data_dir is None:
        raise ValueError('Missing required argument. Must provide either meta '
                         'or data_dir.')

    # Load the metadata
    if meta is None:
        meta = load_skyscan_metadata(data_dir)

    # Get list of files
    prefix = meta['Acquisition']['Filename Prefix']
    img_format = meta['Acquisition']['Image Format']
    if img_format == 'TIFF':
        ext = '.tif'
    else:
        raise RuntimeError(f'Unsupported image format: {img_format}')
    image_paths = sorted([f for f in Path(data_dir).iterdir() if
                          re.search(f'^{prefix}\\d+{ext}$', str(f.name))])
    expected_images = meta['Acquisition']['Number of Files']
    if len(image_paths) != expected_images:
        raise RuntimeError(
            f'Expected {expected_images} images, but found {len(image_paths)}')

    # Get acquisition geometry parameters
    gantry_step = meta['Acquisition']['Rotation Step (deg)']
    source_to_sample = meta['Acquisition']['Object to Source (mm)']
    source_to_detector = meta['Acquisition']['Camera to Source (mm)']
    pix_size = meta['System']['Camera Pixel Size (um)'] / 1000.

    # Scale the sensor pixel size
    if scale is not None:
        pix_size /= scale

    # Load the poses and the images (optional)
    projectors = []
    for idx in tqdm(range(offset, expected_images, skip)):
        # Calculate source position
        up = (0, 0, 1)
        src = np.array((0, -source_to_sample, 0), dtype=np.float32)
        theta = np.deg2rad(idx * gantry_step)
        src = rotate(theta, up) @ src

        # Get center of projection plane
        n = normalize(-src)
        lookat = src + n * source_to_detector

        # Load images if requested
        img = None
        size = None
        if load_images:
            fname = image_paths[idx]
            img = np.array(imageio.imread(fname))
            if scale is not None:
                img = rescale(img, scale=scale, anti_aliasing=True)
            else:
                img = img_as_float(img)

            # Crop to middle row(s)
            if single_slice:
                start = (img.shape[0] - 1) // 2
                if start % 2 == 0:
                    img = img[start:start + 2]
                else:
                    img = img[start:start + 1]

            # Reshape image
            s = (img.shape[0], img.shape[1], 1)
            img = img.reshape(s).astype(np.float32)
        else:
            width = meta['Acquisition']['Number of Columns']
            height = meta['Acquisition']['Number of Rows']
            if scale is not None:
                width = np.round(width * scale).astype(int)
                height = np.round(height * scale).astype(int)
            size = (height, width)

        # Add projector
        cam = ConeBeamProjector(pos=src, lookat=lookat, up=up, image=img,
                                size=size, pixel_size=pix_size)
        projectors.append(cam)

    # Calculate volume size
    vol_min, vol_max = skyscan_volume_size(meta)

    return {
        'projectors': projectors,
        'volume': {
            'min': vol_min,
            'max': vol_max,
            'diameter': np.amax((vol_max - vol_min)[0:2]),
            'height': (vol_max - vol_min)[2]
        },
        'slices': {
            'cols': meta['Acquisition']['Number of Columns'],
            'rows': meta['Acquisition']['Number of Columns'],
            'pix-size': meta['Acquisition']['Image Pixel Size (um)'] / 1000.0
        },
        'metadata': meta
    }


def load_skyscan_render_poses(meta=None, data_dir=None, offset: int = 0,
                              skip: int = 1, scale_factor=None):
    if meta is None and data_dir is None:
        raise ValueError('Missing required argument. Must provide either meta '
                         'or data_dir.')

    # Load the metadata
    if meta is None:
        meta = load_skyscan_metadata(data_dir)

    # Collect the poses
    num_images = meta['Acquisition']['Number of Files']
    gantry_step = meta['Acquisition']['Rotation Step (deg)']
    source_to_sample = meta['Acquisition']['Object to Source (mm)']
    poses = []
    for idx in range(offset, num_images, skip):
        poses.append(spherical_pose(idx * gantry_step, 0, source_to_sample))

    # image size
    height = meta['Acquisition']['Number of Rows']
    width = meta['Acquisition']['Number of Columns']

    # scaled focal length
    focal = meta['Acquisition']['Camera to Source (mm)']
    if scale_factor is not None:
        focal *= scale_factor
        height = np.floor(height * scale_factor).astype(int)
        width = np.floor(width * scale_factor).astype(int)

    # calculate near and far
    # radius of volume in um is scaled pixel size * image width / 2
    # convert to mm by dividing by 1000
    # TODO: radius of volume needs to be adjusted for oversize & offset scans
    half_w = meta['Acquisition']['Number of Columns'] / 2
    vol_radius = meta['Acquisition']['Image Pixel Size (um)'] * half_w / 1000
    near = source_to_sample - vol_radius
    far = source_to_sample + vol_radius

    poses_shape = (len(poses), height, width, 1)
    return {
               'focal_length': focal,
               'near': near,
               'far': far,
               'render': {
                   'poses': np.array(poses),
                   'shape': poses_shape,
               },
           }, meta


def load_skyscan_slices(meta=None, data_dir=None, **kwargs):
    if meta is None and data_dir is None:
        raise ValueError('Missing required argument. Must provide either meta '
                         'or data_dir.')

    # Load the metadata
    if meta is None:
        meta = load_skyscan_metadata(data_dir)

    # Check for reconstruction section
    if 'Reconstruction' not in meta.keys():
        raise RuntimeError(f'Skyscan log does not have reconstruction metadata')

    # Get the volume properties
    start_idx = meta['Reconstruction']['First Section']
    end_idx = meta['Reconstruction']['Last Section']
    padding = meta['File name convention']['Filename Index Length']
    filename_prefix = meta['File name convention']['Filename Prefix']
    file_format = meta['Reconstruction']['Result File Type'].lower()

    volume = []
    data_dir = Path(data_dir)
    for idx in tqdm(range(start_idx, end_idx + 1), 'Loading slices'):
        fname = data_dir / f'{filename_prefix}{idx:0{padding}}.{file_format}'
        img = np.array(imageio.imread(fname))
        volume.append(img)
    return np.expand_dims(volume, axis=3)
