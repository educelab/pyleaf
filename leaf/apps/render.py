import logging
import re
import sys
from datetime import datetime as dt, timezone as tz
from pathlib import Path

import bitmath
import configargparse
import numpy as np
import torch
from bitmath import Byte
from torch.utils.data import DataLoader

import leaf.log_utils as log_utils
from leaf.data import PosesDataset, SlicesDataset, build_spherical_render_poses
from leaf.datasets.skyscan import load_skyscan_metadata, \
    load_skyscan_render_poses
from leaf.models import restore_model_from_checkpoint
from leaf.rendering import LinearRaySampler, render_images, render_rays, \
    render_voxels


def natural_key(string):
    """
    See https://blog.codinghorror.com/sorting-for-humans-natural-sort-order/
        https://stackoverflow.com/a/3033342
    """
    return [int(s) if s.isdecimal() else s for s in
            re.split(r'(\d+)', str(string))]


def create_custom_poses(meta, radius, theta_start, theta_end, theta_steps,
                        phi_start, phi_end, phi_steps, scale_factor=None):
    if radius is None:
        radius = meta['Acquisition']['Object to Source (mm)']

    # image size
    height = meta['Acquisition']['Number of Rows']
    width = meta['Acquisition']['Number of Columns']

    # scaled focal length
    focal = meta['Acquisition']['Camera to Source (mm)']
    if scale_factor is not None:
        focal *= scale_factor
        height = np.floor(height * scale_factor).astype(int)
        width = np.floor(width * scale_factor).astype(int)

    # calculate near and far
    # radius of volume in um is scaled pixel size * image width / 2
    # convert to mm by dividing by 1000
    # TODO: radius of volume needs to be adjusted for oversize/offset scans
    half_w = meta['Acquisition']['Number of Columns'] / 2
    vol_radius = meta['Acquisition'][
                     'Image Pixel Size (um)'] * half_w / 1000
    source_to_sample = meta['Acquisition']['Object to Source (mm)']
    near = source_to_sample - vol_radius
    far = source_to_sample + vol_radius

    poses = build_spherical_render_poses(theta_start, theta_end,
                                         theta_steps, phi_start,
                                         phi_end, phi_steps,
                                         radius)
    poses_shape = (len(poses), height, width, 1)
    data = {
        'focal_length': focal,
        'near': near,
        'far': far,
        'render': {
            'poses': poses,
            'shape': poses_shape,
        }
    }
    return data


def create_slices(meta, centers=None, extents=None, width=None, height=None,
                  scale_factor=None):
    # (default) Slice centered volume origin
    if centers is None:
        centers = [[0, 0, 0]]

    # (default) Dimensions from original slice
    if height is None:
        height = meta['Acquisition']['Number of Rows']
        if scale_factor is not None:
            height = np.floor(height * scale_factor).astype(int)
    if width is None:
        width = meta['Acquisition']['Number of Columns']
        if scale_factor is not None:
            width = np.floor(width * scale_factor).astype(int)

    # (default) Slice volumetric extents from meta, using w/h aspect
    if extents is None:
        pix_size = meta['Acquisition']['Image Pixel Size (um)']
        if scale_factor is not None:
            pix_size /= scale_factor
        ew = pix_size * width / 1000
        eh = pix_size * height / 1000
        extents = [[ew, eh]]

    data = {
        'centers': np.array(centers, dtype=np.float32),
        'extents': np.array(extents, dtype=np.float32),
        'width': width,
        'height': height
    }

    return data


def main():
    parser = configargparse.ArgumentParser(prog='pyleaf-render')
    parser.add_argument('-c', '--config', is_config_file=True,
                        help='Config file path')
    parser.add_argument('-i', '--input', type=str, required=True,
                        help='Path to input dataset')
    parser.add_argument('-o', '--output', type=str,
                        help='Path to output directory for results. If not '
                             'specified, a results directory will be '
                             'created in the current working directory')
    parser.add_argument('-n', '--name', type=str, help='Experiment name.')
    parser.add_argument('--checkpoint', type=str,
                        help='Use a specific checkpoint file. If not '
                             'specified, will search the experiment directory '
                             'for the most recent checkpoint')
    parser.add_argument('--type', type=str.lower,
                        choices=['dataset', 'slice', 'custom'],
                        default='dataset',
                        help='The type of render to generate. A dataset render '
                             'produces projections corresponding to original '
                             'CT projections. A custom render produces '
                             'projections from a custom rotational path')
    parser.add_argument('--log', type=str,
                        help='Write experiment log to a specific file')

    # Dataset render options
    dataset_args = parser.add_argument_group('dataset render options')
    dataset_args.add_argument('--scale', type=float,
                              help='Scale dataset images by the given factor. '
                                   'NOTE: This currently defines the output '
                                   'image size for both the dataset and custom '
                                   'render types, and the automatic size for'
                                   'the slice render type.')
    dataset_args.add_argument('--offset', type=int, default=0,
                              help='When loading a dataset, ignore the first '
                                   '--offset images')
    dataset_args.add_argument('--skip', type=int, default=1,
                              help='When loading a dataset, only include every '
                                   '--skip images')

    # Slice render options
    slice_args = parser.add_argument_group('slice render options')
    slice_args.add_argument('--center', type=float, nargs=3,
                            default=[0., 0., 0.],
                            help='Render a slice centered on the provided '
                                 'origin in volumetric space.')
    slice_args.add_argument('--extents', type=float, nargs=2,
                            help='Set the extents of the rendered slice in '
                                 'volumetric space. If not set, will be '
                                 'auto-calculated from the input dataset.')
    slice_args.add_argument('--slice-width', type=int,
                            help='Set the width of the rendered slice in '
                                 'pixels. If not set, will be auto-calculated '
                                 'from the input dataset.')
    slice_args.add_argument('--slice-height', type=int,
                            help='Set the height of the rendered slice in '
                                 'pixels. If not set, will be auto-calculated '
                                 'from the input dataset.')

    # Custom render options
    custom_args = parser.add_argument_group('custom render options')
    custom_args.add_argument('--theta-start', type=float, default=-180,
                             help='Spherical render path start theta')
    custom_args.add_argument('--theta-end', type=float, default=180,
                             help='Spherical render path end theta')
    custom_args.add_argument('--theta-steps', type=int, default=40,
                             help='Spherical render path number of steps along '
                                  'the theta axis')
    custom_args.add_argument('--phi-start', type=float, default=-30,
                             help='Spherical render path start phi')
    custom_args.add_argument('--phi-end', type=float, default=-30,
                             help='Spherical render path end phi')
    custom_args.add_argument('--phi-steps', type=int, default=1,
                             help='Spherical render path number of steps along '
                                  'the phi axis')
    custom_args.add_argument('--radius', type=float,
                             help='Spherical render path camera distance from '
                                  'origin')

    # Inference options
    inference_args = parser.add_argument_group('inference options')
    inference_args.add_argument('-b', '--batch-size', type=int, default=1024,
                                help='Mini-batch size')
    inference_args.add_argument('-s', '--samples', type=int, default=64,
                                help='Number of ray samples')

    # Logging options
    logging_ops = parser.add_argument_group('logging options')
    logging_ops.add_argument('--buffer-size', type=str, default='100M',
                             help='Pixel buffer size. Rendered images will be '
                                  'written to disk when the pixel buffer is '
                                  'full or the rendering job is complete.')
    logging_ops.add_argument('--log-interval', type=int, default=50,
                             help='Report render progress every N batches')

    # Parse arguments
    args, unknown_args = parser.parse_known_args()

    # Setup experiment
    experiment_start = dt.now(tz.utc)
    datetime_str = experiment_start.strftime('%Y%m%d%H%M%S')
    if args.name is None:
        args.name = datetime_str + '_' + str(Path(args.input).stem)

    # Setup output directory
    if args.output is None:
        args.output = 'results/'
    experiment_output_dir = Path(args.output) / args.name
    log_dir = experiment_output_dir / 'logs'
    conf_dir = experiment_output_dir / 'configs'
    model_dir = experiment_output_dir / 'models'
    renders_dir = experiment_output_dir / 'renders' / datetime_str
    for d in [experiment_output_dir, log_dir, conf_dir, model_dir, renders_dir]:
        d.mkdir(parents=True, exist_ok=True)

    # Setup logging
    if args.log is None:
        args.log = str(
            log_dir / (datetime_str + '_' + args.name + '_render_log.txt'))
    log_utils.setup_app_handlers(log_file=args.log)
    logger = logging.getLogger('leaf.render')
    logger.debug(f'Set up output directory: {experiment_output_dir}')

    # Get device
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    # Report CUDA usage
    logger.info(f'PyTorch device: {device}')
    if device.type == 'cuda':
        logger.info(f'\t{torch.cuda.get_device_name(0)}')
        logger.info(
            f'\tMemory Allocated: '
            f'{round(torch.cuda.memory_allocated(0) / 1024 ** 3, 1)} GB')
        logger.info(
            f'\tMemory Cached:    '
            f'{round(torch.cuda.memory_reserved(0) / 1024 ** 3, 1)} GB')
    pin_mem = device.type == 'cuda'

    # Load checkpoint
    if args.checkpoint is None:
        checkpoints = sorted(model_dir.rglob('checkpoint*.tar'),
                             key=natural_key)
        if len(checkpoints) == 0:
            logger.error('No checkpoint found in experiment directory and '
                         '--checkpoint option not provided.')
            sys.exit(1)
        args.checkpoint = checkpoints[-1]
    logger.info(f'Loading checkpoint file: {args.checkpoint}')
    checkpoint = torch.load(args.checkpoint, map_location=device)

    # Create model
    model, optimizer = restore_model_from_checkpoint(checkpoint, device)
    logger.info(f'Restored model:\n'
                f'\t- Model Class: {model.__class__.__name__}\n'
                f'\t- {model.num_input_cns} inputs -> {model.num_output_cns} outputs\n'
                f'\t- Embedder: {model.configuration["embedder"]}\n'
                f'\t- Hidden Layers: {model.num_hidden_layers}\n'
                f'\t- Channels: {model.num_hidden_channels}\n'
                f'\t- Activation Fn: {model.configuration["activation"]}')
    model.eval()

    # Load datasets
    logger.info(f'Loading dataset metadata: {args.input}')
    meta = load_skyscan_metadata(args.input)

    # Construct poses
    if args.type == 'dataset':
        data = load_skyscan_render_poses(meta=meta,
                                         scale_factor=args.scale,
                                         offset=args.offset,
                                         skip=args.skip)[0]
    elif args.type == 'slice':
        # Convert to list of lists
        if args.center is not None:
            args.center = [args.center]
        if args.extents is not None:
            args.extents = [args.extents]
        data = create_slices(meta=meta,
                             centers=args.center,
                             extents=args.extents,
                             width=args.slice_width,
                             height=args.slice_height,
                             scale_factor=args.scale)

    elif args.type == 'custom':
        data = create_custom_poses(meta=meta,
                                   radius=args.radius,
                                   theta_start=args.theta_start,
                                   theta_end=args.theta_end,
                                   theta_steps=args.theta_steps,
                                   phi_start=args.phi_start,
                                   phi_end=args.phi_end,
                                   phi_steps=args.phi_steps,
                                   scale_factor=args.scale)
    else:
        logger.error(f'Unrecognized render type: {args.type}')
        sys.exit(1)

    # Setup torch datasets and loaders
    info_extra = ''
    if args.type in ['dataset', 'custom']:
        data_shape = data['render']['shape']
        dataset = PosesDataset(poses=data['render']['poses'],
                               height=data_shape[1], width=data_shape[2],
                               focal_length=data['focal_length'],
                               near=data['near'], far=data['far'])
        dataset.ray_sampler = LinearRaySampler(num_samples=args.samples)
        render_fn = render_rays

    # Slice dataset
    else:
        slices = data['centers'].shape[0]
        height = data['height']
        width = data['width']
        data_shape = (slices, height, width)
        dataset = SlicesDataset(centers=data['centers'],
                                extents=data['extents'],
                                height=height,
                                width=width)
        render_fn = render_voxels
        info_extra = f'\n' \
                     f'\t- Center: {data["centers"][0]}\n' \
                     f'\t- Extents: {data["extents"][0]}'

    # Report loaded dataset
    logger.info(f'Render info:\n'
                f'\t- Type: {args.type}\n'
                f'\t- Render shape: {data_shape}'
                f'{info_extra}')

    # Setup persistent data loaders
    dataloader = DataLoader(dataset=dataset, batch_size=args.batch_size,
                            pin_memory=pin_mem)

    # Write config after all arguments have been changed
    config = conf_dir / (datetime_str + '_' + args.name + '_render_config.txt')
    args.config = str(config)
    logger.debug(f'Writing config file: {args.config}')
    with config.open(mode='w') as file:
        for arg in vars(args):
            attr = getattr(args, arg)
            arg = arg.replace('_', '-')
            file.write('{} = {}\n'.format(arg, attr))

    # Setup buffer flush size
    try:
        buffer_max = np.floor(
            bitmath.parse_string_unsafe(args.buffer_size) / Byte(4)).astype(int)
    except ValueError as e:
        logger.error(e)
        sys.exit(1)
    logger.debug(
        f'Pixel buffer size: '
        f'{(buffer_max * Byte(4)).best_prefix().format("{value:.3f}{unit}")}, '
        f'length: {buffer_max} pixels')

    # Iterate
    logger.info('Begin rendering')
    render_start = dt.now(tz.utc)
    images = None
    buffer = None
    interrupted = False
    output_path = renders_dir / f'render_final.tif'

    try:
        images, buffer = render_images(dataloader=dataloader, device=device,
                                       model=model, shape=data_shape,
                                       render_fn=render_fn,
                                       buffer_max=buffer_max,
                                       flush_path=output_path,
                                       log_interval=args.log_interval)
    except KeyboardInterrupt:
        logger.warning(f'Rendering interrupted.')
        interrupted = True
    else:
        logger.info(f'Rendering complete.')

    # Always do one last buffer flush
    if not interrupted:
        if buffer is not None and buffer.size > 0:
            logger.warning(f'{buffer.size} pixels still in pixel buffer')

        # Log info
        logger.info(f'Rendering stats:\n'
                    f'\tImages rendered: {images.shape[0]}\n'
                    f'\tTotal rendering time: '
                    f'{(dt.now(tz.utc) - render_start).total_seconds()}s')


if __name__ == '__main__':
    logger = logging.getLogger('leaf.render')
    logger.error('This program has not been updated to support the models '
                 'produced by pyleaf-recon. Disabled until this is fixed.')
    sys.exit(1)
