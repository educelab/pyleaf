# LeAF: Learned Attenuation Fields

A modification of the [NeRF](https://github.com/bmild/nerf) model to address tomographic data.

[[_TOC_]]

## Requirements

This project requires Python 3.8 or 3.9 and is built around PyTorch and Torchvision. It automatically uses a single 
CUDA device if one is detected on the system and is properly configured. Full project requirements can be found in
[requirements.txt](requirements.txt) or [setup.cfg](setup.cfg).

## Installation

Before installing pyleaf, it is highly recommended that you set up some sort of Python virtual environment. The authors
use [venv](https://docs.python.org/3/library/venv.html):

```bash
# Create venv
python3 -m venv venv/

# Activate venv
source venv/bin/activate
```

### With pip (PyPI)

Coming soon.

### With pip (from source)

```bash
python -m pip install -e https://code.cs.uky.edu/seales-research/pyleaf.git
```

## Usage

Coming soon.

## Singularity

We provide [a singularity definition file](singularity/pyleaf.def) for generating a container equipped with a full
pyleaf installation:

```bash
# Create a copy of the source code in /tmp/pyleaf/
git clone https://code.cs.uky.edu/seales-research/pyleaf.git /tmp/pyleaf/

# Build the container
sudo singularity build pyleaf.sif pyleaf.def
```

Pyleaf commands are automatically aliased into the container's PATH and are available via`singularity run` or
`singularity shell`. Additionally, the container is configured with overlay support so that you may update the pyleaf
source code without rebuilding the container:

```bash
# Run the pyleaf-recon script
singularity run pyleaf.sif pyleaf-recon [...]

# Make an overlay file and open a singularity shell w/overlay
dd if=/dev/zero of=pyleaf.overlay bs=1M count=500 && /sbin/mkfs.ext3 -F pyleaf.overlay
singularity shell --overlay pyleaf.overlay pyleaf.sif

# Change to pyleaf source code directory inside container
cd /usr/local/educelab/pyleaf

# Update the source code and exit the shell
git pull
exit

# Run reports a new version
singularity run pyleaf.sif pyleaf-recon [...]
```