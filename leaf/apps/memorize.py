import logging
import re
import sys
from collections import Counter, OrderedDict
from datetime import datetime as dt, timezone as tz
from pathlib import Path
from typing import List

import bitmath
import configargparse
import imageio
import numpy as np
import skimage.data
import torch
import torchsummary
from bitmath import Byte
from scipy import ndimage
from skimage.util import img_as_float32
from torch.nn import ReLU, Sequential
from torch.optim.lr_scheduler import LinearLR
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter

import leaf.app_utils as app_utils
import leaf.log_utils as log_utils
from leaf.data import ImageCoordinateDataset, ImageDataset, MultiResImageDataset
from leaf.datasets.phantoms import load_shepp_logan_slices
from leaf.datasets.skyscan import load_skyscan_slices
from leaf.embedding import (GaussianEmbedding, MinMaxScalingEmbedder,
                            PassThruEmbedder, PositionalEncoder)
from leaf.models import Leaf
from leaf.modules import Sin, Swish
from leaf.rendering import render_images, render_voxels

SHAPE_REGEX = r"(?P<n>-*\d+)x(?P<h>-*\d+)x(?P<w>-*\d+)"
ORIGIN_REGEX = r"\+(?P<z>\d+)\+(?P<y>\d+)\+(?P<x>\d+)"
CROP_REGEX = r"(?P<index>\d+)," + SHAPE_REGEX + ORIGIN_REGEX
SHAPE_REGEX = re.compile(SHAPE_REGEX)
ORIGIN_REGEX = re.compile(ORIGIN_REGEX)
CROP_REGEX = re.compile(CROP_REGEX)


class MaxBatchReached(Exception):
    """Indicates that the user-defined batch maximum has been reached"""


def load_phantom(name):
    # Check for phantom protocol
    if name.find('phantom://') != 0:
        raise RuntimeError(f'Unrecognized argument: {name}')

    # Remove prefix
    phantom = name.replace('phantom://', '')

    if phantom == 'brain':
        return np.expand_dims(skimage.data.brain(), axis=3)
    elif phantom == 'camera':
        return np.expand_dims(skimage.data.camera(), axis=(0, 3))
    elif phantom == 'astronaut':
        img = skimage.data.astronaut()
        return np.expand_dims(img, axis=0)
    elif phantom == 'shepp-logan':
        img = load_shepp_logan_slices(modified=False)
        return np.expand_dims(img, axis=3)
    elif phantom == 'shepp-logan-modified':
        img = load_shepp_logan_slices()
        return np.expand_dims(img, axis=3)
    else:
        raise RuntimeError(f'Unrecognized phantom dataset: {phantom}')


def apply_layout(img, layout: str):
    logger = logging.getLogger('leaf.memorize')

    # Make sure layout matches what we have
    if img.ndim != len(layout):
        logger.error('Number of image and layout dimensions don\'t match')
        raise RuntimeError('Number of image and layout dimensions don\'t match')

    # Target layout
    target = ['N', 'H', 'W', 'C']

    # Make sure there aren't duplicate or unrecognized assignments
    errors = []
    counter = Counter(layout)
    for key, count in counter.items():
        if key not in target:
            errors.append(("unrecognized", key, count))
        elif count > 1:
            errors.append(("duplicate", key, count))
    if len(errors):
        msg = f'Detected unrecognized or duplicate layout parameters:\n '
        for m, k, c in errors:
            msg += f'\t - "{k}" detected {c} times ({m})\n'
        logger.error(msg)
        raise RuntimeError('Detected unrecognized/duplicate layout parameters')

    # Calculate permutation
    permutation = [layout.find(c) for c in target]

    # Add missing dimensions
    new_dims = []
    idx_delta = 0
    for idx, val in enumerate(permutation):
        if val == -1:
            new_dims.append(idx)
            permutation[idx] = idx
            idx_delta += 1
        else:
            permutation[idx] = val + idx_delta
    if len(new_dims):
        logger.debug(f'Adding new input dimensions: {new_dims}')
        img = np.expand_dims(img, axis=new_dims)

    # Apply transpose
    logger.debug(f'Permuting input dimensions: {permutation}')
    return np.transpose(img, axes=permutation)


def load_image(input_path, layout=None, scale=None):
    """Returns [z, y, x, c]"""
    logger = logging.getLogger('leaf.memorize')
    if re.search(r'^phantom://.*', input_path):
        img = img_as_float32(load_phantom(input_path))
    elif Path(input_path).is_dir():
        img = img_as_float32(load_skyscan_slices(data_dir=input_path))
    else:
        # Load the image
        if Path(input_path).suffix in ['.png', '.jpg']:
            img = imageio.imread(input_path)
        else:
            img = imageio.mvolread(input_path)
        img = img_as_float32(img)
        # If 2D, convert to 4D
        if img.ndim == 2 and layout is None:
            layout = 'HW'
        # If 3D, convert to 4D
        elif img.ndim == 3 and layout is None:
            layout = 'HWC'
        # Apply layout if we have one
        if layout is not None:
            img = apply_layout(img, layout)
    if scale is not None:
        logger.info(f'Scaling image by {scale}x')
        img = ndimage.zoom(img, zoom=(scale, scale, scale, 1), order=3,
                           prefilter=scale < 1)
    return img


def parse_crop_params(crops: List[str]):
    logger = logging.getLogger('leaf.memorize')

    if crops is None:
        return {}

    # Parse crop commands
    crop_commands = {}
    for crop in crops:
        # Parse the crop parameters
        match = CROP_REGEX.match(crop)
        if not match:
            logger.warning(f'Cannot parse crop argument: {crop}. Skipping.')
            continue

        # Convert to ints
        crop_params = match.groupdict()
        convert_error = False
        for key, value in crop_params.items():
            try:
                crop_params[key] = int(value)
                if key != 'index' and crop_params[key] <= 0:
                    crop_params[key] = None
            except ValueError as err:
                convert_error = True
                logger.warning(f'{err}. Skipping crop command: {crop}')
                break
        if convert_error:
            continue

        # Add to list of crop commands
        crop_commands[crop_params['index']] = crop_params

    return crop_commands


def parse_shape(shape: str):
    logger = logging.getLogger('leaf.memorize')

    # Parse the shape parameters
    match = SHAPE_REGEX.match(shape)
    if not match:
        logger.warning(f'Cannot parse shape argument: {shape}. Ignoring.')
        return None, None, None, None

    shape = match.groupdict()
    for key, value in shape.items():
        try:
            shape[key] = int(value)
            if shape[key] <= 0:
                shape[key] = None
        except ValueError as err:
            logger.warning(f'{err}. Ignoring shape: {shape}')
            break

    return shape['n'], shape['h'], shape['w'], None


def parse_origin(origin: str):
    logger = logging.getLogger('leaf.memorize')

    # Parse the shape parameters
    match = ORIGIN_REGEX.match(origin)
    if not match:
        logger.warning(f'Cannot parse origin argument: {origin}. Ignoring.')
        return None, None, None

    origin = match.groupdict()
    for key, value in origin.items():
        try:
            origin[key] = int(value)
            if origin[key] <= 0:
                origin[key] = None
        except ValueError as err:
            logger.warning(f'{err}. Ignoring origin: {origin}')
            break

    return origin['z'], origin['y'], origin['x']


def main():
    # Parse arguments
    parser = configargparse.ArgumentParser(prog='pyleaf-memorize')

    # Generic options
    parser.add_argument('-c', '--config', is_config_file=True,
                        help='Config file path')
    parser.add_argument('-i', '--input', type=str,
                        default='phantom://camera',
                        help='Path to input image. To use a built-in '
                             'phantom, use the phantom:// prefix (e.g. '
                             '-i phantom://camera)')
    parser.add_argument('-o', '--output', type=str,
                        help='Path to output directory for results. If not '
                             'specified, a results directory will be '
                             'created in the current working directory')
    parser.add_argument('-n', '--name', type=str, help='Experiment name.')
    parser.add_argument('--log', type=str,
                        help='Write experiment log to a specific file. If not '
                             'specified, log will be written to output '
                             'directory')
    parser.add_argument('--input-layout', type=str.upper,
                        help='Layout of input image dimensions '
                             '(e.g. NHWC, CHW)')

    # Dataset options
    dataset_args = parser.add_argument_group('dataset options')
    dataset_args.add_argument('--scale', type=float,
                              help='Scale input image by the given factor')

    # Multi-resolution training
    multires_args = parser.add_argument_group('multi-res options')
    multires_args.add_argument('--add-scale', type=float, action='append',
                               dest='scales',
                               help='Enable multi-resolution training, scale '
                                    'the input image by X, and append the '
                                    'resulting image to the training set.')
    multires_args.add_argument('--crop', action='append', dest='crops',
                               help='Crop parameters of the form '
                                    '\'I,NxHxW+Z+Y+X\'. I specifies the index '
                                    'of the training image to be cropped. '
                                    'NxHxW specify the new dimensions of the '
                                    'cropped image. +Z+Y+X specify the origin '
                                    'of the cropped image in original image '
                                    'coordinates. This is similar to the way '
                                    'ImageMagick specifies cropping.')

    # Model options
    network_args = parser.add_argument_group('model options')
    network_args.add_argument('-d', '--depth', type=int, default=8,
                              help='Number of hidden layers')
    network_args.add_argument('-w', '--width', type=int, default=256,
                              help='Number of neurons in each hidden layer')
    network_args.add_argument('--activation', type=str.lower, default='relu',
                              choices=['relu', 'sin', 'swish'],
                              help='Hidden layer activation function')
    network_args.add_argument('--sin-w0', type=float, default='30',
                              help='When using Sin activation, set the value '
                                   'of the weights in the first layer')
    network_args.add_argument('--embedder', type=str.lower, default='posenc',
                              choices=['none', 'minmax', 'posenc', 'gaussian'],
                              help='Point embedding function')
    network_args.add_argument('--posenc-freqs', type=int, default=10,
                              help='Number of frequency components in the '
                                   'posenc embedder')
    network_args.add_argument('--gaussian-freqs', type=int, default=256,
                              help='Number of frequency components generated '
                                   'by the gaussian embedder.')
    network_args.add_argument('--gaussian-scale', type=float, default=10.,
                              help='Std. deviation of the distribution used '
                                   'by the gaussian embedder')

    # Training options
    training_args = parser.add_argument_group('training options')
    training_args.add_argument('-e', '--epochs', type=int, default=1000,
                               help='Training epochs')
    training_args.add_argument('-b', '--batch-size', type=int, default=1024,
                               help='Mini-batch size')
    training_args.add_argument('--batches', type=int,
                               help='If specified, stop training after a '
                                    'maximum of N batches')
    training_args.add_argument('--learning-rate', type=float, default=1.e-3,
                               help='Optimizer learning rate')
    training_args.add_argument('--scheduler-epochs', type=int,
                               help='When provided, decay the learning rate '
                                    'over N epochs')

    # Tensorboard options
    tb_args = parser.add_argument_group('tensorboard options')
    tb_args.add_argument('--tb-train-summary', type=int, default=10,
                         help='Log training summary to Tensorboard every N '
                              'batches')
    tb_args.add_argument('--render-image', type=int, default=100,
                         help='Log a test image to Tensorboard every N batches')
    tb_args.add_argument('--render-shape',
                         help='Size of the render image in the form \'NxHxW\'')
    tb_args.add_argument('--render-origin',
                         help='Origin of the render image in original image '
                              'units. Takes the form \'+Z+Y+X\'')
    tb_args.add_argument('--render-stride', type=float, default=1.,
                         help='Sample distance between render pixels in '
                              'original image units.')
    tb_args.add_argument('--buffer-size', type=str, default='100M',
                         help='Pixel buffer size. Rendered images will be '
                              'written to disk when the pixel buffer is '
                              'full or the rendering job is complete.')

    args, unknown_args = parser.parse_known_args()

    # Setup experiment
    experiment_start = dt.now(tz.utc)
    datetime_str = experiment_start.strftime('%Y%m%d%H%M%S')
    if args.name is None:
        args.name = datetime_str + '_' + str(Path(args.input).stem)

    # Setup output directory
    if args.output is None:
        args.output = 'results/'
    experiment_output_dir = Path(args.output) / args.name
    log_dir = experiment_output_dir / 'logs'
    conf_dir = experiment_output_dir / 'configs'
    tensorboard_dir = experiment_output_dir / 'tensorboard' / datetime_str
    renders_dir = experiment_output_dir / 'renders' / datetime_str
    for d in [experiment_output_dir, log_dir, conf_dir,
              tensorboard_dir, renders_dir]:
        d.mkdir(parents=True, exist_ok=True)

    # Setup logging
    if args.log is None:
        args.log = str(
            log_dir / (datetime_str + '_' + args.name + '_memorize_log.txt'))
    log_utils.setup_app_handlers(log_file=args.log)
    logger = logging.getLogger('leaf.memorize')
    logger.info(f'Set up output directory: {experiment_output_dir}')

    # Setup buffer flush size
    try:
        buffer_max = np.floor(
            bitmath.parse_string_unsafe(args.buffer_size) / Byte(4)).astype(int)
    except ValueError as e:
        logger.error(e)
        sys.exit()
    logger.debug(
        f'Pixel buffer size: '
        f'{(buffer_max * Byte(4)).best_prefix().format("{value:.3f}{unit}")}, '
        f'length: {buffer_max} pixels')

    # Get device
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    pin_mem = device.type == 'cuda'

    # Load image
    logger.info(f'Loading image: {args.input}')
    img = load_image(args.input, layout=args.input_layout, scale=args.scale)
    logger.info(f'Loaded image: {img.shape}')

    if img.size > buffer_max:
        buffer_max = img.size
        buffer_bytes = (buffer_max * Byte(4)).best_prefix()
        buffer_bytes = buffer_bytes.format('{value:.3f}{unit}')
        args.buffer_size = buffer_bytes
        logger.warning(f'Pixel buffer is not large enough to hold the input '
                       f'dataset. Expanding pixel buffer to size: '
                       f'{buffer_bytes}, length: {buffer_max} pixels')

    # Setup activation function
    if args.activation == 'sin':
        activation = Sin
    elif args.activation == 'swish':
        activation = Swish
    else:
        activation = ReLU

    # Setup min-max embedder
    x_min = torch.tensor([0., 0., 0.])
    x_max = torch.tensor([img.shape[2], img.shape[1], img.shape[0]])
    a = torch.full((3,), -torch.pi)
    b = torch.full((3,), torch.pi)
    minmax = MinMaxScalingEmbedder(x_min=x_min, x_max=x_max, a=a, b=b)

    # Setup full embedder
    if args.embedder == 'minmax':
        embedder = minmax
    elif args.embedder == 'posenc':
        posenc = PositionalEncoder(freqs=args.posenc_freqs)
        embedder = Sequential(OrderedDict([
            ('minmax', minmax),
            ('posenc', posenc)
        ]))
    elif args.embedder == 'gaussian':
        a = torch.full((3,), 0.)
        b = torch.full((3,), 2. * torch.pi)
        minmax = MinMaxScalingEmbedder(x_min=x_min, x_max=x_max, a=a, b=b)
        gauss = GaussianEmbedding(scale=args.gaussian_scale,
                                  output_dims=args.gaussian_freqs)
        embedder = Sequential(OrderedDict([
            ('minmax', minmax),
            ('gauss', gauss)
        ]))
    else:
        embedder = PassThruEmbedder()

    # Create model
    model = Leaf(output_cns=img.shape[3],
                 hidden_layers=args.depth, hidden_cns=args.width,
                 embedder=embedder, activation_fn=activation,
                 sin_w0=args.sin_w0).to(device)

    # Setup optimizer
    optimizer = torch.optim.Adam(params=model.parameters(),
                                 lr=args.learning_rate)
    scheduler = None
    if args.scheduler_epochs is not None:
        scheduler = LinearLR(optimizer=optimizer,
                             total_iters=args.scheduler_epochs)

    # Setup loss
    loss_fn = torch.nn.MSELoss()

    # Preallocate model buffers
    pts = torch.rand((args.batch_size, model.num_input_cns),
                     device=device)
    pixels = torch.rand((args.batch_size, img.shape[3]), device=device)
    loss = render_voxels(pts=pts, model=model, expected=pixels,
                         loss_fn=loss_fn)["loss"]
    loss.backward()
    model.zero_grad(set_to_none=True)

    # Report CUDA usage
    logger.info(f'PyTorch device: {device}')
    if device.type == 'cuda':
        logger.info(f'\t{torch.cuda.get_device_name(0)}')
        logger.info(
            f'\tMemory Allocated: {round(torch.cuda.memory_allocated(0) / 1024 ** 3, 1)} GB')
        logger.info(
            f'\tMemory Cached:    {round(torch.cuda.memory_reserved(0) / 1024 ** 3, 1)} GB')

    # Report model summary
    input_size = (args.batch_size, model.num_input_cns)
    summary = torchsummary.summary(model=model,
                                   input_data=input_size,
                                   branching=False,
                                   device=device,
                                   verbose=0)
    logger.info(
        f'Model summary (sizes represent a single training batch):\n'
        f'{summary}')

    # Setup datasets
    if args.scales is not None and len(args.scales):
        args.scales.insert(0, 1.0)
        crops = parse_crop_params(args.crops)
        dataset = MultiResImageDataset(img, scales=args.scales, crops=crops)
    else:
        dataset = ImageDataset(img)
    loader = DataLoader(dataset=dataset, batch_size=args.batch_size,
                        shuffle=True, pin_memory=pin_mem)

    # Setup render origin
    render_origin = (0., 0., 0.)
    if args.render_origin is not None:
        new_origin = parse_origin(args.render_origin.strip('\'"'))
        render_origin = tuple(n if n is not None else o for n, o in
                              zip(new_origin, render_origin))

    # Setup render shape
    render_shape = img.shape
    if args.render_shape is not None:
        new_shape = parse_shape(args.render_shape.strip('\'"'))
        render_shape = tuple(
            n if n is not None else o for n, o in zip(new_shape, render_shape))

    coords = ImageCoordinateDataset(shape=render_shape, origin=render_origin,
                                    stride=args.render_stride)
    render_loader = DataLoader(dataset=coords, batch_size=args.batch_size,
                               pin_memory=pin_mem)

    writer = SummaryWriter(log_dir=str(tensorboard_dir))
    with torch.no_grad():
        pts, *_ = next(iter(loader))
        writer.add_graph(model, pts.to(device))
    if img.shape[3] > 4:
        logger.warning('Loaded image has more than 4 channels. Renders will '
                       'not be logged to Tensorboard.')
    else:
        writer.add_images(tag='Image/expected',
                          img_tensor=img,
                          global_step=0,
                          dataformats='NHWC')

    # Write config after all arguments have been changed
    config = conf_dir / (
            datetime_str + '_' + args.name + '_memorize_config.txt')
    args.config = str(config)
    app_utils.write_config(config, args)

    # configuration
    total_batches = len(loader) * args.epochs
    if args.batches is not None:
        total_batches = min(total_batches, args.batches)
    total_batch_digits = len(str(total_batches))
    epoch_log_interval = 10
    batch_log_interval = 10

    # Train
    try:
        logger.info('Begin training')
        for epoch in range(args.epochs):
            for batch_idx, (pts, pixels) in enumerate(loader):
                # Calculate global batch idx
                all_batch_idx = epoch * len(loader) + batch_idx
                if args.batches is not None and all_batch_idx == args.batches:
                    raise MaxBatchReached()
                msg_prefix = f'[{all_batch_idx:>{total_batch_digits}d}/' \
                             f'{total_batches:<{total_batch_digits}d}] '

                # Train on the batch
                optimizer.zero_grad(set_to_none=True)
                loss = render_voxels(pts=pts.to(device), model=model,
                                     expected=pixels.to(device),
                                     loss_fn=loss_fn)["loss"]

                # Backprop
                loss.backward()
                optimizer.step()

                # Checks for non-parameterized logging
                need_epoch_log = epoch % epoch_log_interval == 0 and batch_idx == 0
                need_batch_log = all_batch_idx % batch_log_interval == 0

                # Print training summary to console/log on interval
                if all_batch_idx < 10 or need_epoch_log or need_batch_log:
                    logger.info(msg_prefix +
                                f'Epoch: {epoch:>5d}/{args.epochs:<5d} '
                                f'Batch: {batch_idx:>5d}/{len(loader):<5d} '
                                f'Loss: {loss:>7f}')

                # Write training summary to tensorboard
                if all_batch_idx % args.tb_train_summary == 0:
                    writer.add_scalar(tag='Loss/train',
                                      scalar_value=loss,
                                      global_step=all_batch_idx)

                # Render the image
                if all_batch_idx % args.render_image == 0 or all_batch_idx == total_batches - 1:
                    render_file = f'learned_image_e{epoch}_b{batch_idx}.tif'
                    render_path = renders_dir / render_file
                    render_start = dt.now()
                    image, buffer = render_images(dataloader=render_loader,
                                                  device=device, model=model,
                                                  shape=render_shape,
                                                  render_fn=render_voxels,
                                                  buffer_max=buffer_max,
                                                  flush_path=render_path)
                    render_duration = (dt.now() - render_start).total_seconds()
                    image = np.reshape(image, render_shape)
                    if img.shape[3] <= 4:
                        writer.add_images(tag='Image/learned',
                                          img_tensor=image,
                                          global_step=all_batch_idx,
                                          dataformats='NHWC')
                    logger.info(msg_prefix +
                                'Rendered image '
                                f'({render_duration:5.3g}s)')
            # Step if a scheduler is initialized
            if scheduler is not None:
                scheduler.step()
    except KeyboardInterrupt:
        logger.warning(f'Training interrupted.')
    except MaxBatchReached:
        logger.info(f'Training stopped. Max batch reached.')
    else:
        logger.info(f'Training complete.')


if __name__ == '__main__':
    main()
