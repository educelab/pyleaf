import logging
import time
from typing import List


class IncludeExcludeFilter(logging.Filter):
    def __init__(self, include: List[str] = None, exclude: List[str] = None):
        super(IncludeExcludeFilter, self).__init__()
        self._include = include
        self._exclude = exclude

    def filter(self, record):
        # Accept immediately if substr is present
        if self._include is not None:
            for a in self._include:
                if a in record.name:
                    return True

        # Reject immediately if substr is present
        if self._exclude is not None:
            for a in self._exclude:
                if a in record.name:
                    return False

        # If we have an include list, reject everything else
        if self._include is None:
            return True
        else:
            return False


def setup_app_handlers(log_file='log.txt'):
    # Setup formatter
    logger_frmt = logging.Formatter(
        fmt='[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S %Z')
    logger_frmt.converter = time.gmtime

    # Setup filter
    log_filter = IncludeExcludeFilter(include=['leaf.'])

    # Setup handlers
    handlers = []

    stderr_hndl = logging.StreamHandler()
    stderr_hndl.setLevel(logging.INFO)
    stderr_hndl.setFormatter(logger_frmt)
    stderr_hndl.addFilter(log_filter)
    handlers.append(stderr_hndl)

    if log_file is not None:
        file_hndl = logging.FileHandler(filename=log_file, mode='w')
        file_hndl.setLevel(logging.DEBUG)
        file_hndl.setFormatter(logger_frmt)
        file_hndl.addFilter(log_filter)
        handlers.append(file_hndl)

    date_format = '%Y-%m-%d %H:%M:%S %Z'
    line_format = '[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s'
    # noinspection PyArgumentList
    logging.basicConfig(format=line_format, level=logging.DEBUG,
                        datefmt=date_format, handlers=handlers)
