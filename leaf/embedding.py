import numpy as np
import torch
import torch.nn as nn

from leaf.modules import Cos, Sin


class PassThruEmbedder(nn.Identity):
    def __init__(self):
        super(PassThruEmbedder, self).__init__()

    @property
    def input_cns(self):
        return 3

    @property
    def output_cns(self):
        return 3


class MinMaxScalingEmbedder(nn.Module):
    def __init__(self, x_min, x_max, a, b):
        super(MinMaxScalingEmbedder, self).__init__()
        from torch.nn import Parameter
        self._input_cns = self._output_cns = x_min.shape[0]
        self._x_min = Parameter(x_min, requires_grad=False)
        self._x_range = Parameter(torch.sub(x_max, self._x_min),
                                  requires_grad=False)
        self._a = Parameter(a, requires_grad=False)
        self._ab_range = Parameter(torch.sub(b, a), requires_grad=False)

    @property
    def input_cns(self):
        return self._input_cns

    @property
    def output_cns(self):
        return self._output_cns

    def forward(self, x):
        # a + (x - min_x) * (b - a) / (max_x - min_x)
        x = self._a + (x - self._x_min) * self._ab_range / self._x_range
        return x


class PositionalEncoder(nn.Module):
    """Frequency embedding function from original NeRF paper"""

    def __init__(self, input_cns=3, include_input=True, freqs=10):
        super(PositionalEncoder, self).__init__()

        self._input_cns = input_cns
        self._include_input = include_input
        self._num_freqs = freqs
        self._max_freq = freqs - 1
        self._periodic_fns = [Sin, Cos]
        self._log_sampling = True

        self._embed_fns = nn.ModuleList()
        self._output_cns = 0
        if include_input:
            self._embed_fns.append(nn.Identity())
            self._output_cns += self._input_cns

        if self._log_sampling:
            freq_bands = 2. ** np.linspace(0., self._max_freq, self._num_freqs)
        else:
            freq_bands = np.linspace(2. ** 0., 2. ** self._max_freq,
                                     self._num_freqs)

        for idx, freq in enumerate(freq_bands):
            for p_fn in self._periodic_fns:
                self._embed_fns.append(p_fn(period=freq))
                self._output_cns += self._input_cns

    @property
    def input_cns(self):
        return self._input_cns

    @property
    def output_cns(self):
        return self._output_cns

    def forward(self, x):
        return torch.cat([f(x) for f in self._embed_fns], -1)


class GaussianEmbedding(nn.Module):
    """
    Gaussian embedding function from 'Fourier Features Let Networks Learn
    High Frequency Functions in Low Dimensional Domains', Tancik et al. [2020]
    """

    def __init__(self, input_dims=3, output_dims=256, scale=10.):
        super(GaussianEmbedding, self).__init__()

        self._input_cns = input_dims
        self._output_cns = output_dims * 2

        self._fns = nn.ModuleList([Sin(), Cos()])
        mean = torch.zeros((input_dims, output_dims))
        std = torch.full((input_dims, output_dims), scale)
        b = torch.normal(mean=mean, std=std)
        self._b = nn.Parameter(b, requires_grad=False)

    @property
    def input_cns(self):
        return self._input_cns

    @property
    def output_cns(self):
        return self._output_cns

    def forward(self, x):
        x = torch.cat([f(x @ self._b) for f in self._fns], -1)
        return x
