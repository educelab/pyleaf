import argparse
import logging
import random

import imageio
import numpy as np
import phantominator as ph
from scipy.interpolate import interp1d
from tqdm import tqdm

from leaf.log_utils import setup_app_handlers


def main():
    # Parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--output-file', type=str, required=True,
                        help='Output file path')
    parser.add_argument('--type', type=str.lower, choices=['2d', '3d'],
                        default='2d', help='Generate a 2D or 3D cube')
    parser.add_argument('--dim', type=int, default=192,
                        help='Edge length of the phantom in pixels')
    parser.add_argument('--bands', type=int, default=10,
                        help='Number of energy bands to generate')
    parser.add_argument('--degree', type=int, default=1,
                        help='Interpolation degree')
    parser.add_argument('--num-control-points', type=int, default=0)
    parser.add_argument('--random-control-points', action='store_true')
    args = parser.parse_args()

    # Setup loggers
    setup_app_handlers(log_file=None)
    logger = logging.getLogger('leaf.generate_multienergy')

    # Setup 2D or 3D
    if args.type == '2d':
        params = ph.ct_modified_shepp_logan_params_2d()
        high_params = ph.ct_shepp_logan_params_2d()
        band_shape = (args.dim,) * 2
        cube_writer = imageio.mimwrite
    else:
        params = ph.ct_modified_shepp_logan_params_3d()
        high_params = ph.ct_shepp_logan_params_3d()
        band_shape = (args.dim,) * 3
        cube_writer = imageio.mvolwrite

    # Interpolate the intensity parameters
    logger.info('Interpolating intensity parameters')
    t0 = params[:, 0]
    t1 = high_params[:, 0]

    # Generate more control points if we're interpolating a polynomial
    xs = []
    ys = []
    pts = []
    if args.degree > 1:
        # Need degree - 1 more cps
        bin_width = 1 / (args.degree - 1)
        min_p = 1 / (args.degree + 1)
        for idx in range(0, max(args.num_control_points, args.degree - 1)):
            if args.random_control_points:
                # Random non-zero normalized x position
                p = max(min_p,
                        random.uniform(bin_width * idx, bin_width * (idx + 1)))
                # Random t for interpolating t0->t1
                t = random.random()
            else:
                # Linear x position and t value
                p = t = (bin_width * idx + bin_width * (idx + 1)) / 2
            pts.append((p, t))
            # Calculate x in range [1.001, bands)
            xs.append(1 + p * args.bands)
            # Interpolate y between [t0, t1)
            ys.append(t0 + t * ((t1 - t0) / args.bands))
    interp = interp1d(x=[1, *xs, args.bands], y=np.vstack([t0, *ys, t1]),
                      axis=0, kind=args.degree)
    ts = interp(range(1, args.bands + 1))

    # Generate each band
    bands = []
    for row in tqdm(ts, desc='Generating energy bands'):
        params[:, 0] = row
        band = ph.ct_shepp_logan(band_shape, E=params)
        bands.append(band.astype(np.float32))

    # Save final cube
    logger.info(f'Saving cube to file: {args.output_file}')
    cube = np.array(bands, dtype=np.float32)
    cube_writer(args.output_file, cube)


if __name__ == '__main__':
    main()
