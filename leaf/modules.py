import torch
from torch import Tensor, sigmoid
from torch.nn import Module, Parameter


class Sin(Module):
    r"""Applies the element-wise function:

        .. math::
            \text{Sin}(x) = \sin(x)

        Shape:
            - Input: :math:`(N, *)` where `*` means, any number of additional
              dimensions
            - Output: :math:`(N, *)`, same shape as the input
        """

    def __init__(self, amplitude=1., period=1., phase=0.):
        super(Sin, self).__init__()
        self._amplitude = Parameter(torch.tensor(amplitude),
                                    requires_grad=False)
        self._period = Parameter(torch.tensor(period), requires_grad=False)
        self._phase = Parameter(torch.tensor(phase), requires_grad=False)

    def forward(self, x: Tensor) -> Tensor:
        # period
        x = x * self._period
        # phase shift
        x = x + self._phase
        # sin
        x = torch.sin(x)
        # amplitude
        x = x * self._amplitude
        return x


class Cos(Module):
    r"""Applies the element-wise function:

        .. math::
            \text{Cos}(x) = \cos(x)

        Shape:
            - Input: :math:`(N, *)` where `*` means, any number of additional
              dimensions
            - Output: :math:`(N, *)`, same shape as the input
        """

    def __init__(self, amplitude=1., period=1., phase=0.):
        super(Cos, self).__init__()
        self._amplitude = Parameter(torch.tensor(amplitude),
                                    requires_grad=False)
        self._period = Parameter(torch.tensor(period), requires_grad=False)
        self._phase = Parameter(torch.tensor(phase), requires_grad=False)

    def forward(self, x: Tensor) -> Tensor:
        # period
        x = x * self._period
        # phase shift
        x = x + self._phase
        # cos
        x = torch.cos(x)
        # amplitude
        x = x * self._amplitude
        return x


class Transform3D(Module):
    r"""A learnable 4x4 transform matrix for 3D systems:

        Shape:
            - Input: :math:`(N,)` or :math:`(B, N)` where `N` is the dimension
            of the vector (either 3 or 4) and `B` is the batch. If the input
            shape is 1-dimensional, it is expanded to a batch with a single
            vector. If `N` is 3, each vector is expanded with a one in a 4th
            column for matrix multiplication.
            - Output: same shape as the input
        """

    def __init__(self, mtx: Tensor = None, requires_grad=True):
        super(Transform3D, self).__init__()
        if mtx is not None:
            assert mtx.shape == (4, 4)
            self._mtx = Parameter(mtx, requires_grad=requires_grad)
        else:
            self._mtx = Parameter(torch.eye(4), requires_grad=requires_grad)

    def forward(self, x: Tensor) -> Tensor:
        # batch a single vector input
        if x.dim() == 1:
            x = torch.unsqueeze(x, 0)

        # turn 3D vectors into 4D vectors
        input_3d = False
        if x.shape[-1] == 3:
            input_3d = True
            x = torch.cat((x, torch.ones((x.shape[0], 1))), -1)

        # apply transform
        x = (self._mtx @ x.T).T

        # If inputs were 3D, turn them back into 4D
        if input_3d:
            x = x[:, :3]

        # Remove any dims of 1
        torch.squeeze(x)

        return x


class Swish(Module):
    r"""Applies the element-wise function:

        .. math::
            \text{Swish}(x) = x * sigmoid(\beta x)

        Shape:
            - Input: :math:`(N, *)` where `*` means, any number of additional
              dimensions
            - Output: :math:`(N, *)`, same shape as the input
        """

    def __init__(self, beta: Tensor = torch.tensor([1.]), requires_grad=False):
        super(Swish, self).__init__()
        if type(beta) is not Tensor:
            beta = torch.tensor(beta)
        assert torch.numel(beta) == 1
        self._b = Parameter(beta, requires_grad=requires_grad)

    def forward(self, x):
        return x * sigmoid(self._b * x)
